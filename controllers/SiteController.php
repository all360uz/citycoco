<?php

namespace app\controllers;

use app\models\Articles;
use app\models\Contacts;
use app\models\forms\Login;
use app\models\GalleryImages;
use app\models\MainFilter;
use app\models\PhotoGallery;
use app\models\Products;
use app\models\ProductSortAttrs;
use app\models\SellersContact;
use app\models\Testdrive;
use app\models\VideoGallery;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\ContactForm;
use app\models\WidgetCarousel;
use app\models\Categories;
use yii\data\Pagination;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'header_404',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $carusel = WidgetCarousel::find()
            ->where(['status' => 1])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        $category = Categories::find()
            ->joinWith(['productBrands' => function ($query) {
                $query->joinWith('products');
            }])
            ->where(['categories.status' => 1])
            ->all();
        $new_products = Products::find()
            ->joinWith('productImages' )
            ->where(['status' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->limit('4')
            ->all();
        $popular_products = Products::find()->distinct()
            ->where(['status' => 1])
            ->orderBy(['rating' => SORT_DESC])
            ->joinWith('productImages')
            ->limit(10)
            ->all();
        $main_categories = Categories::find()
            ->where(['is_main_page' => 1, 'status' => 1])
            ->all();
        $video = VideoGallery::find()
            ->orderBy(['id' => SORT_DESC])
            ->limit(3)
            ->all();
        $main_filter = MainFilter::find()
            ->joinWith(['products'])
            ->where(['main_filter.status' => 1])
            ->all();

        return $this->render('index', compact([
            'carusel',
            'category',
            'new_products',
            'popular_products',
            'main_categories',
            'video',
            'main_filter',
        ]));

    }

    public function actionFilterProds()
    {
        $id = Yii::$app->request->get('id');
        $category = Yii::$app->request->get('category');
        if (Yii::$app->request->isAjax) {
            $products = Products::find()
                ->joinWith(['productsMainFilters' => function ($query) use ($id) {
                $query->where(['main_filter_id' => $id]);
            }])
                ->andWhere(['category_id' => $category])
                ->all();
            return $this->renderAjax('filter-prods-ajax', [
                'products' => $products,
            ]);
        }
    }

    public function actionGetProds($id)
    {
        $popular_products = Products::find()
            ->filterWhere(['status' => 1])
            ->andFilterWhere(['category_id' => $id])
            ->orderBy(['rating' => SORT_DESC])
            //->joinWith('productImages')
            ->limit(10)
            ->all();

        return $this->renderAjax('get-prods-ajax', [
            'popular_products' => $popular_products,
        ]);
    }


    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionVideo()
    {
        $query = VideoGallery::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 12, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $video = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('video', compact('pages', 'video'));
    }

    public function actionService()
    {
        $service = Articles::find()
            ->orderBy(['id' => SORT_ASC])
            ->limit(1)
            ->one();
        $services = Articles::find()
            ->where(['<>','id', $service->id ])
            ->all();
        return $this->render('service', compact('service','services'));
    }

    public function actionServiceView($id)
    {
        $service = Articles::find()
            ->where(['id' => $id])
            ->one();
        $services = Articles::find()
            ->all();
        return $this->render('service-view', compact('service','services', 'id'));
    }

    public function actionContacts()
    {
        $contacts = SellersContact::find()->all();
        $contact = Contacts::find()->where(['id' => 1])->one();
        return $this->render('contacts', compact('contacts', 'contact'));

    }

    public function actionTestdrive()
    {
        $test = Testdrive::find()
            ->where(['id' => 1])
            ->one();
        $popular_products = Products::find()
            ->joinWith('productImages')
            ->where(['status' => 1])
            ->orderBy(['rating' => SORT_DESC])
            ->limit(6)
            ->all();
        return $this->render('testdrive', compact('test', 'popular_products'));
    }

    public function actionGallery()
    {

        $query = PhotoGallery::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 12, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $gallery = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('gallery', compact('gallery','pages'));
    }

    public function actionPhotos($id)
    {
        $query = GalleryImages::find()->where(['gallery_id' => $id]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 20, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $photos = $query->offset($pages->offset)->limit($pages->limit)->all();
        $gallery = PhotoGallery::findOne($id);
        return $this->render('photos', compact('photos', 'pages','gallery'));

    }



}
