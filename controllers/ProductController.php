<?php
/**
 * Created by PhpStorm.
 * User: Nazirov
 * Date: 23.03.2018
 * Time: 22:14
 */

namespace app\controllers;

use app\models\AddFilter;
use app\models\DeliveryTypes;
use app\models\MainFilter;
use app\models\Orders;
use app\models\OrdersProducts;
use app\models\PaymentTypes;
use app\models\ProductReview;
use app\models\ProductSortAttrs;
use app\models\ProductsSearch;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Products;
use app\models\Categories;
use app\models\Cart;
use app\models\UserDeliveryAddress;
use app\models\Sms;
use yii\web\Response;
use yii\widgets\ActiveForm;

class ProductController extends Controller
{


    public function actionProduct($slug)
    {
        $product = Products::find()
            ->where(['slug' => $slug])
            ->one();
        $query = ProductReview::find()
            ->where(['product_id' => $product->id]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 8, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $reviews = $query->offset($pages->offset)->limit($pages->limit)->all();
        $count_reviews = !empty($query) ? $query->count() : '0';

        return $this->render('product', compact([
            'product',
            'reviews',
            'pages',
            'count_reviews',
        ]));

    }

    public function actionCategory($id)
    {
       // Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        $brands = Yii::$app->request->get('brands');
        $recommend = Yii::$app->request->get('our_recommend');
        $custom_filter = Yii::$app->request->get('add_filter');

        if (!$brands)
            $brands = [];
        if (!$recommend)
            $recommend = [];
        if (!$custom_filter)
            $custom_filter = [];

        $sort = Yii::$app->request->get('sort');
        if($sort == null){  $sort = 'price'; }
        //debug($sort);
        $category = Categories::findOne($id);
        $categories = Categories::find()->where(['status' => 1])->all();
        $query = Products::find()->distinct()
            ->orderBy([$sort => SORT_DESC])
            ->joinWith('mainFilters')
            ->joinWith('addFilters')
            ->filterWhere(['in', 'brand_id', $brands])
            ->filterWhere(['in', 'main_filter.id', $recommend])
            ->filterWhere(['in', 'products_add_filter.add_filter_id', $custom_filter])
            ->andFilterWhere(['products.status' => 1])
            ->andFilterWhere(['category_id' => $id]);
        $pages = new Pagination([
            'totalCount' => $query->count(),
            'pageSize' => 16,
            'forcePageParam' => false,
            'pageSizeParam' => false
        ]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        $related_product = Products::find()
            ->where(['category_id' => $id, 'status' => 1])
            ->orderBy(['rating' => SORT_DESC])
            ->limit(1)
            ->one();
        $sort = ProductSortAttrs::find()->where(['status' => 1])->all();
        $main_filter = MainFilter::find()->where(['status' => 1])->all();
        $add_filter = AddFilter::find()->all();


        return $this->render('category', compact([
            'category',
            'categories',
            'id',
            'products',
            'pages',
            'related_product',
            'sort',
            'main_filter',
            'add_filter',
            'recommend',
            'brands',
            'custom_filter',
        ]));
    }

    public function actionCatFilter($id)
    {
        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        $brands = Yii::$app->request->get('brands');
        $recommend = Yii::$app->request->get('our_recommend');
        $custom_filter = Yii::$app->request->get('add_filter');
        $sort = Yii::$app->request->get('sort');

        if($sort == null){  $sort = 'price'; }
        $query = Products::find()
            ->distinct()
            ->joinWith('mainFilters')
            ->joinWith('addFilters')
            ->joinWith('addFilters')
            ->filterWhere(['in', 'brand_id', $brands])
            ->filterWhere(['in', 'main_filter.id', $recommend])
            ->filterWhere(['in', 'products_add_filter.add_filter_id', $custom_filter])
            ->andFilterWhere(['products.status' => 1])
            ->andFilterWhere(['category_id' => $id]) ;

        $pages = new Pagination([
            'totalCount' => $query->count(),
            'pageSize' => 16,
            'forcePageParam' => false,
            'pageSizeParam' => false
        ]);
        $products = $query->orderBy([$sort => SORT_DESC])
            ->offset($pages->offset)->limit($pages->limit)->all();

        return [
            'content' => $this->renderAjax('cat-filter-ajax', compact([
                'products',
            ])),
            'url' => Url::current(['product/category']),
        ];
    }




    public function actionAddReview()
    {
        $id = Yii::$app->request->post('id');
        $rate = Yii::$app->request->post('rate');
        $reviw = ProductReview::find()->where(['product_id' => $id])->all();
        $rating = 0;
        foreach ($reviw as $item):
            $rating += $item->rate;
        endforeach;
        $count = count($reviw) + 1;
        $model = new ProductReview();
        $model->name = Yii::$app->request->post('name');
        $model->review = Yii::$app->request->post('review');
        $model->rate = $rate;
        $model->created_at = time();
        $model->updated_at = time();
        $model->product_id = $id;
        $model->save();


        $product = Products::findOne($id);
        if ($count == 0): $product->rating = $rate;
        else: $product->rating = ($rating + $rate) / $count; endif;
        $product->save();
        $slug = $product->slug;
        return $this->render('addreview', compact('slug'));
    }

    public function actionCartadd($id)
    {

        $qty = (int)Yii::$app->request->get('qty');
        $qty = !$qty ? 1 : $qty;
        $product = Products::findOne($id);
        if (empty($product)) return false; // елси пусто
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->addToCart($product, $qty);
        return true;
    }

    public function actionCartMinus($id)
    {

        $qty = (int)Yii::$app->request->get('qty');
        $qty = !$qty ? 1 : $qty;
        $product = Products::findOne($id);
        if (empty($product)) return false; // елси пусто
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->minusToCart($product, $qty);
        return true;
    }

    public function actionCart()
    {
        $delivery = DeliveryTypes::find()->where(['status' => 1])->One();
        $session = Yii::$app->session;
        $session->open();
        //debug($session['cart']);
        return $this->render('cart', compact(['session', 'delivery']));
    }

    public function actionDelItem($id)
    {            // Удалить по кнопке

        $id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalc($id);
        //$this->layout = false;

        return true;
    }

    public function actionSearch($q = null)
    {

//        $q = trim(Yii::$app->request->get('q'));
        if ($q == null) return $this->render('search');
        $query = Products::find()->filterWhere(['like', 'name', $q]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 12, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('search', compact([
                'products',
                'pages',
                'q']
        ));

    }

    public function actionLog()
    {
        $model = new Sms();
        return $this->render('login', compact('model'));
    }

    public function actionLogin()
    {
        $model = new Sms();

        if (Yii::$app->request->post('phone')) {
            $model->phone = '998' . Yii::$app->request->post('phone');
            $model->sms = '123456';
            $model->send_at = time();
            $model->save();

            return $this->render('checksms', compact('model'));

        } else {
            print_r(Yii::$app->request->post());
        }
    }

    public function actionChecksms($id)
    {

        if(Yii::$app->request->post()){

           // Yii::$app->response->format = Response::FORMAT_JSON;
            $sms = Yii::$app->request->post('sms');
            $model = Sms::findOne($id);

            if($model->sms === $sms)
            {
                return $this->render('address', compact('model'));
            }
            else {

                 return $this->render('address', compact('model'));
            }


        }
        else{
            return $this->render('login');

        }

    }

    public function actionAddress($id)
    {
        $user_address = new UserDeliveryAddress();
        $delivery_type = DeliveryTypes::find()->all();
        if(Yii::$app->request->post('place')){
            $phone = Sms::findOne($id)->phone;
            $user_address->place = Yii::$app->request->post('place');
            $user_address->address = Yii::$app->request->post('address');
            $user_address->lat = Yii::$app->request->post('lat');
            $user_address->lng = Yii::$app->request->post('lng');
            $user_address->phone = $phone;
            $user_address->save();
            return $this->render('delivery-type', compact('user_address', 'delivery_type'));
        }
        else{

            print_r(Yii::$app->request->post());
        }
    }


    public function actionUpdateAddress($id){

        $user_delivery = UserDeliveryAddress::findOne($id);

        return $this->render('update-address', compact('user_delivery'));


    }

    public function actionAddressChanged($id)
    {
        $user_address = UserDeliveryAddress::findOne($id);
        if(Yii::$app->request->post())
        {
            $user_address->place = Yii::$app->request->post('place');
            $user_address->address = Yii::$app->request->post('address');
            $user_address->lat = Yii::$app->request->post('lat');
            $user_address->lng = Yii::$app->request->post('lng');
            $user_address->save();
            $delivery_type = DeliveryTypes::find()->all();
            return $this->render('delivery-type', compact('user_address', 'delivery_type'));
        }
    }

    public function actionPay($id)
    {
        $user_address = UserDeliveryAddress::findOne($id);
        $phone = $user_address->phone;
        $session = Yii::$app->session;
        $session->open();
        $orders = new Orders();

        if(Yii::$app->request->post('d_method')){

            $orders->amount = $session['cart.sum'];
            $orders->address_id = $id;
            $orders->start_date = time();
            $orders->updated_date = time();
            $orders->phone = $phone;
            $orders->delivery_type = Yii::$app->request->post('d_method');

            if($orders->save(false)){

                $this->saveOrderItems($session['cart'], $orders->id); // запись на Order Products
                $session->remove('cart');
                $session->remove('cart.sum');
                $session->remove('cart.qty');
                $payment_types = PaymentTypes::find()->where(['status' => 1])->all();
             return $this->render('pay', compact('orders','payment_types','user_address'));
            }


        }

    }

    protected function saveOrderItems ($items, $order_id) {         // Запись всех продуктов из Order
        foreach ($items as $id => $item) {
            $order_items = new OrdersProducts();

            if(!empty($item['discount'])):
                $price = $item['discount'];
            else:
                $price = $item['price'];
            endif;

            $order_items->orders_id = $order_id;
            $order_items->products_id = $item['id'];
            $order_items->quantity = $item['qty'];
            $order_items->discount = $price * $item['qty'];
            $order_items->product_price = $price;
            $order_items->save();
        }
    }


    public function actionPayment($id) {

        $order = Orders::findOne($id);

        if(Yii::$app->request->post('payment_type')){
            $order->payment_type = Yii::$app->request->post('payment_type');
            $order->save(false);

            return $this->render('thanks');


        }
        else{

            debug($order);
        }

    }


}