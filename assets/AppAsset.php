<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main.css',
        'css/defaultStyles.css',
        'css/animated.css',
        'css/fonts.css',
        'css/owl.carousel.min.css',
        'css/owl.theme.default.min.css',
        'css/magicscroll.css',
        'css/magiczoom.css',
        'css/jquery.formstyler.css',
        'css/jquery.fancybox.min.css',
        'css/jquery.mCustomScrollbar.css',
        'css/style.css',
        'css/media.css',
//        'css/site.css',

    ];
    public $js = [
       // 'js/jquery.js',
        'js/main.js',
        'js/owl.carousel.js',
        'js/magicscroll.js',
        'js/magiczoom.js',
        'js/jquery.formstyler.js',
        'js/jquery.fancybox.min.js',
        'js/jquery.mCustomScrollbar.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
