<div class="mainContainer">
    <div class="contentPadding">
        <h1 class="title-1"><?= $gallery->name;?></h1>
        <div class="photoAlbum">
            <ul>
                <?php if(!empty($photos)): ?>
                    <?php foreach ($photos as $item): ?>
                <li>
                    <a href="/uploads/<?= $item->img; ?>" data-fancybox="gallery" data-caption="<?= $gallery->name;?>" class="fancybox">
                        <div style="background-image: url(/uploads/<?= $item->img; ?>);" class="imgBox">
                        </div>
                    </a>
                </li>
                    <?php endforeach;?>
                <?php else:?>
                    <h3 style="margin: 15px">Фотографии еще не добавлены</h3>
                <?php endif;?>
            </ul>
        </div>
        <div class="paginator">

            <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
                'options' => ['class' => false],
                'prevPageLabel' => false,
                'nextPageLabel' => false,
                'activePageCssClass' => 'activeItem',

            ]);?>

        </div>
    </div>
</div>