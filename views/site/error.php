<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?><div class="mainContainer">
    <div class="contentPadding">
        <div class="page404">
            <div class="page404Img"><img src="/images/404.png" alt=""/></div>
            <h1 class="page404H1">Страница не найдена</h1><a href="/" class="btn0 btn1 uppercase">На главную</a>
        </div>
    </div>
</div>