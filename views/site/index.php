<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Главая';
?>

<div class="mainContainer padding0">
    <div id="mainBlock">
        <div id="mainSlider" class="owl-carousel owl-theme">
            <?php if (!empty($carusel)): ?>
                <?php foreach ($carusel as $item): ?>
                    <div class="mainSliderItem"><a href="<?= $item->url; ?>">
                            <div class="sliderPTitle"><?= $item->small_caption; ?></div>
                            <div class="sliderPPrice"><?= $item->main_caption; ?></div>
                            <div class="sliderPImg">
                                <?= Html::img(Yii::getAlias('@web') . '/uploads/' . $item->img) ?>
                            </div>
                            <div class="sliderLink">Смотреть</div>
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div id="mainBlockNav" class="scrollBar">
            <nav>
                <ul>
                    <?php if (!empty($category)): ?>
                        <?php foreach ($category as $item): ?>
                            <li><a href="#liContent-<?= $item->id; ?>"><?= $item->name; ?></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
        <div id="mainBlockNavContents">
            <?php if (!empty($category)): ?>
                <?php foreach ($category as $item): ?>
                    <div id="liContent-<?= $item->id; ?>" class="navItemContent">
                        <div class="navPCol">
                            <?php if (!empty($item->img)): ?>
                                <a>
                                    <div style="background-image: url('/uploads/<?= $item->img ?>');"
                                         class="imgBox"></div>
                                </a>
                            <?php else: ?>
                                <a href="<?= Url::to(['product/product', 'slug' => $item->relatedProduct->slug]) ?>">
                                    <div style="background-image: url('/uploads/<?= $item->relatedProduct->img ?>');"
                                         class="imgBox"></div>
                                    <div class="navPColData">
                                        <div class="itemTitle"><?= $item->relatedProduct->name ?></div>
                                        <div class="itemStars">
                                            <div class="starsList">
                                                <?php if ($item->relatedProduct->rating != 0): ?>
                                                    <?php for ($i = 0; $i < $item->relatedProduct->rating; $i++): ?>
                                                        <span class="active"></span>
                                                    <?php endfor; ?>
                                                <?php endif; ?>
                                                <?php for ($i = 0; $i < 5 - $item->relatedProduct->rating; $i++): ?>
                                                    <span></span>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                        <div class="itemPrices">
                                            <?php if (!empty($item->relatedProduct->discount)): ?>
                                                <div class="itemPrice"><?= number_format($item->relatedProduct->discount, 0, ' ', ' ') ?>
                                                    СУМ
                                                </div>
                                                <div class="itemOldPrice"><?= number_format($item->relatedProduct->price, 0, ' ', ' ') ?>
                                                    СУМ
                                                </div>
                                            <?php else: ?>
                                                <div class="itemPrice"><?= number_format($item->relatedProduct->price, 0, ' ', ' ') ?>
                                                    СУМ
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div class="navListCol">
                            <?php foreach ($item->productBrands as $item): ?>
                                <nav>
                                    <div class="navListColTitle"><?= $item->name; ?></div>
                                    <ul>
                                        <?php if (!empty($item->products)): ?>
                                            <?php foreach ($item->products as $product): ?>
                                                <li>
                                                    <a href="<?= Url::to(['product/product', 'slug' => $product->slug]) ?>"><?= $product->name; ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </nav>
                            <?php endforeach; ?>
                            <a href="<?= Url::to(['product/category', 'id' => $item->id]) ?>" class="link">Посмотреть
                                все</a>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<div id="mainBlockItems">
    <div class="mainContainer">
        <ul>
            <li>
                <h2 class="navTitle">Новые Продукты</h2>
            </li>
            <?php if (!empty($new_products)): ?>
                <?php foreach ($new_products as $item): ?>
                    <li><a href="<?= Url::to(['product/product', 'slug' => $item->slug]) ?>">
                            <div class="itemImage">
                                <?= Html::img(Yii::getAlias('@web') . '/uploads/' . $item->img) ?>
                            </div>
                            <div class="itemData">
                                <div class="itemTitle"><?= $item->name; ?></div>
                                <div class="itemStars">
                                    <div class="starsList">
                                        <?php if ($item->rating != 0): ?>
                                            <?php for ($i = 0; $i < $item->rating; $i++): ?>
                                                <span class="active"></span>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                        <?php for ($i = 0; $i < 5 - $item->rating; $i++): ?>
                                            <span></span>
                                        <?php endfor; ?>
                                    </div>
                                </div>
                                <div class="itemPrices">
                                    <?php if (!empty($item->discount)): ?>
                                        <div class="itemPrice"><?= number_format($item->discount, 0, ' ', ' ') ?>СУМ
                                        </div>
                                        <div class="itemOldPrice"><?= number_format($item->price, 0, ' ', ' ') ?>СУМ
                                        </div>
                                    <?php else: ?>
                                        <div class="itemPrice"><?= number_format($item->price, 0, ' ', ' ') ?> СУМ</div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </a></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>
<div class="mainContainer">
    <div class="section">
        <div class="sectionHead">
            <div class="sectionSort filter-items">
                <ul>
                    <li class="activeItem"><a href="#" data-id=""><span>Все товары</span></a></li>
                    <?php if (!empty($main_categories)): ?>
                        <?php foreach ($main_categories as $item): ?>
                            <li><a href="javascript:void(0)" data-id="<?= $item->id ?>"><span><?= $item->name; ?></span></a>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
                <div class="sortMore"><a class="sortMoreBtn"><img src="/images/icons/more.svg"/></a>
                    <ul>
                        <li class="activeItem"><a href="#" data-id="">Все товары</a></li>
                        <?php if (!empty($main_categories)): ?>
                            <?php foreach ($main_categories as $item): ?>
                                <li>
                                    <a href="javascript:void(0)" data-id="<?= $item->id ?>">
                                        <span><?= $item->name; ?></span>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <h2 class="sectionTitle">Популярные Продукты</h2>
        </div>
        <div class="productsList">
            <ul>
                <?php if (!empty($popular_products)): ?>
                    <?php foreach ($popular_products as $key => $item): ?>
                        <?php if ($key % 7 == 0 || $key == 0): ?>
                            <li class="itemDuble">
                                <a href="<?= Url::to(['product/product', 'slug' => $item->slug]) ?>">
                                    <div class="itemImage">
                                        <?= Html::img(Yii::getAlias('@web') . '/uploads/' . $item->img) ?>
                                    </div>
                                    <div class="itemData">
                                        <div class="itemTitle"><?= $item->name; ?></div>
                                        <div class="itemStars">
                                            <div class="starsList">
                                                <?php if ($item->rating != 0): ?>
                                                    <?php for ($i = 0; $i < $item->rating; $i++): ?>
                                                        <span class="active"></span>
                                                    <?php endfor; ?>
                                                <?php endif; ?>
                                                <?php for ($i = 0; $i < 5 - $item->rating; $i++): ?>
                                                    <span></span>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                        <div class="itemDesc"><?= substr($item->short_desc, 0, 50); ?>...</div>
                                        <div class="itemPrices">
                                            <?php if (!empty($item->discount)): ?>
                                                <div class="itemPrice"><?= number_format($item->discount, 0, ' ', ' ') ?>
                                                    СУМ
                                                </div>
                                                <div class="itemOldPrice"><?= number_format($item->price, 0, ' ', ' ') ?>
                                                    СУМ
                                                </div>
                                            <?php else: ?>
                                                <div class="itemPrice"><?= number_format($item->price, 0, ' ', ' ') ?>
                                                    СУМ
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?php else: ?>
                            <li><a href="<?= Url::to(['product/product', 'slug' => $item->slug]) ?>">
                                    <div class="itemData">
                                        <div class="itemTitle"><?= $item->name; ?></div>
                                        <div class="itemStars">
                                            <div class="starsList">
                                                <?php if ($item->rating != 0): ?>
                                                    <?php for ($i = 0; $i < $item->rating; $i++): ?>
                                                        <span class="active"></span>
                                                    <?php endfor; ?>
                                                <?php endif; ?>
                                                <?php for ($i = 0; $i < 5 - $item->rating; $i++): ?>
                                                    <span></span>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                        <div class="itemPrices">
                                            <?php if (!empty($item->discount)): ?>
                                                <div class="itemPrice"><?= number_format($item->discount, 0, ' ', ' ') ?>
                                                    СУМ
                                                </div>
                                                <div class="itemOldPrice"><?= number_format($item->price, 0, ' ', ' ') ?>
                                                    СУМ
                                                </div>
                                            <?php else: ?>
                                                <div class="itemPrice"><?= number_format($item->price, 0, ' ', ' ') ?>
                                                    СУМ
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="itemImage">
                                        <?= Html::img(Yii::getAlias('@web') . '/uploads/' . $item->img) ?>
                                    </div>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <?php if (!empty($main_categories)): ?>
        <?php foreach ($main_categories as $cat => $items): ?>

            <div class="section">
                <div class="sectionHead">
                    <div class="sectionSort">
                        <ul>
                            <?php if (!empty($main_filter)): ?>
                                <?php $k = 1;
                                foreach ($main_filter as $filter): ?>
                                    <?php if ($filter->products): ?>
                                        <li class="<?= $k == 1 ? 'activeItem' : '' ?>">
                                            <a href="javascript:void(0)" data-brand="<?= $items->id ?>"
                                               data-filter="<?= $filter->id ?>" class="filter_product">
                                                <span><?= $filter->name; ?></span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <?php $k++; endforeach; ?>
                            <?php endif; ?>
                            <li>
                                <a href="<?= Url::to(['product/category', 'id' => $items->id]) ?>"><span>Смотреть все</span></a>
                            </li>
                        </ul>
                        <div class="sortMore"><a class="sortMoreBtn"><img src="/images/icons/more.svg"/></a>
                            <ul>
                                <?php if (!empty($main_filter)): ?>
                                    <?php foreach ($main_filter as $filter): ?>
                                        <li class="activeItem"><a href="#"><span><?= $filter->name; ?></span></a></li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <li><a href="<?= Url::to(['product/category', 'id' => $items->id]) ?>"><span>Смотреть все</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <h2 class="sectionTitle"><?= $items->name; ?></h2>
                </div>
                <div class="productsList">
                    <ul>

                        <?php if (!empty($items->products)): ?>
                            <?php foreach ($items->products as $key => $product): ?>

                                <?php if ($key == $cat || $key == 7): ?>
                                    <li class="itemDuble"><a
                                                href="<?= Url::to(['product/product', 'slug' => $product->slug]) ?>">
                                            <div class="itemImage"><img src="/uploads/<?= $product->img; ?>"/></div>
                                            <div class="itemData">
                                                <div class="itemTitle"><?= $product->name; ?></div>
                                                <div class="itemStars">
                                                    <div class="starsList">
                                                        <?php if ($product->rating != 0): ?>
                                                            <?php for ($i = 0; $i < $product->rating; $i++): ?>
                                                                <span class="active"></span>
                                                            <?php endfor; ?>
                                                        <?php endif; ?>
                                                        <?php for ($i = 0; $i < 5 - $product->rating; $i++): ?>
                                                            <span></span>
                                                        <?php endfor; ?>
                                                    </div>
                                                </div>
                                                <div class="itemDesc"><?= substr($product->short_desc, 0, 40); ?></div>
                                                <div class="itemPrices">
                                                    <?php if (!empty($product->discount)): ?>
                                                        <div class="itemPrice"><?= number_format($product->discount, 0, ' ', ' ') ?>
                                                            СУМ
                                                        </div>
                                                        <div class="itemOldPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                                            СУМ
                                                        </div>
                                                    <?php else: ?>
                                                        <div class="itemPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                                            СУМ
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </a></li>
                                <?php else: ?>
                                    <li><a href="<?= Url::to(['product/product', 'slug' => $product->slug]) ?>">
                                            <div class="itemImage"><img src="/uploads/<?= $product->img; ?>"/></div>
                                            <div class="itemData">
                                                <div class="itemTitle"><?= $product->name; ?></div>
                                                <div class="itemStars">
                                                    <div class="starsList">
                                                        <?php if ($product->rating != 0): ?>
                                                            <?php for ($i = 0; $i < $product->rating; $i++): ?>
                                                                <span class="active"></span>
                                                            <?php endfor; ?>
                                                        <?php endif; ?>
                                                        <?php for ($i = 0; $i < 5 - $product->rating; $i++): ?>
                                                            <span></span>
                                                        <?php endfor; ?>
                                                    </div>
                                                </div>
                                                <div class="itemPrices">
                                                    <?php if (!empty($product->discount)): ?>
                                                        <div class="itemPrice"><?= number_format($product->discount, 0, ' ', ' ') ?>
                                                            СУМ
                                                        </div>
                                                        <div class="itemOldPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                                            СУМ
                                                        </div>
                                                    <?php else: ?>
                                                        <div class="itemPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                                            СУМ
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </a></li>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        <?php else: ?>
                            <h4 style="margin: 15px">Продукты еще не добалены</h4>
                        <?php endif; ?>

                    </ul>
                </div>
            </div>

        <?php endforeach; ?>
    <?php endif; ?>

</div>
