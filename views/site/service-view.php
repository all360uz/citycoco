<div class="mainContainer">
    <div class="contentPadding">
        <h1 class="title-1">Доставка</h1>
        <div class="postPageRow">
            <div class="postSidebar">
                <ul class="sidebarList">
                    <?php if(!empty($services)):?>
                        <?php foreach ($services as $item): ?>
                            <li class="<?= $id == $item->id ? 'activeItem' : null ?>" ><a href="<?= \yii\helpers\Url::to(['/site/service-view', 'id' => $item->id]);?>"><?= $item->title; ?></a></li>
                        <?php endforeach;?>
                    <?php endif;?>
                </ul>
            </div>
            <div class="postContainer postContainerColl">
                <div class="postContent">
                    <?php if(!empty($service)): ?>
                        <?= $service->content; ?>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>