<div class="mainContainer">
    <div class="contentPadding">
        <div class="postContainer">
            <h1 class="title-1">Тестдрайв</h1>
            <div class="postContent"><?= $test->content; ?></div><br/><br/>
            <div class="defaultSlider">
                <h2 class="title-1">Популярные Продукты</h2>
                <div class="productsList">
                    <ul class="dSlider owl-carousel owl-theme">
                     <?php if (!empty($popular_products)): ?>
                        <?php foreach($popular_products as $item): ?>
                        <li>
                            <a href="#">
                                <div class="itemData">
                                    <div class="itemTitle"><?= $item->name;?></div>
                                    <div class="itemStars">
                                        <div class="starsList">
                                            <?php if ($item->rating != 0): ?>
                                                <?php for($i = 0; $i < $item->rating; $i++):?>
                                                    <span class="active"></span>
                                                <?php endfor; ?>
                                            <?php endif; ?>
                                            <?php for($i = 0; $i < 5-$item->rating; $i++):?>
                                                <span></span>
                                            <?php endfor; ?>
                                        </div>
                                    </div>
                                    <div class="itemPrices">
                                        <?php if(!empty($item->discount)):?>
                                            <div class="itemPrice"><?=number_format($item->discount, 0, ' ', ' ')?> СУМ</div>
                                            <div class="itemOldPrice"><?=number_format($item->price, 0, ' ', ' ')?> СУМ</div>
                                        <?php else: ?>
                                            <div class="itemPrice"><?=number_format($item->price, 0, ' ', ' ')?> СУМ</div>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="itemImage"><img src="/uploads/<?=$item->img;?>"/>
                                </div>
                            </a>
                        </li>
                         <?php endforeach; ?>
                     <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>