<div class="mainContainer">
    <div class="contentPadding">
        <h1 class="title-1">Галерея</h1>
        <div class="mediaList galleryList">
            <ul>
                <?php if(!empty($gallery)):?>
                 <?php foreach($gallery as $item): ?>
                <li>
                    <div class="mediaImg"><a href="<?= \yii\helpers\Url::to(['site/photos/', 'id' => $item->id])?>">
                            <div style="background-image: url(/uploads/<?=$item->img;?>);" class="imgBox"></div></a></div>
                    <div class="mediaData">
                        <div class="itemDate"><?= date('M d, Y',$item->created_at);?></div>
                        <div class="itemLargeTitle"><?=$item->name;?></div>
                        <div class="itemDesc"><?=$item->description;?></div>
                    </div>
                </li>
                 <?php endforeach;?>
                <?php endif;?>
            </ul>
        </div>
        <div class="paginator">

            <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
                'options' => ['class' => false],
                'prevPageLabel' => false,
                'nextPageLabel' => false,
                'activePageCssClass' => 'activeItem',

            ]);?>

        </div>
    </div>
</div>