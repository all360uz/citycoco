<div class="mainContainer">
    <div class="contentPadding">
        <h1 class="title-1">Видео</h1>
        <div class="mediaList">
            <ul>
                <?php if(!empty($video)):?>
                  <?php foreach ($video as $item): ?>
                <li>
                    <div class="mediaImg" style="margin-bottom: -6px">
                            <iframe width="420" height="243" frameborder="0"
                                    src="<?= $item->url; ?>">
                            </iframe>
                    </div>
                    <div class="mediaData">
                        <div class="itemLargeTitle"><?= $item->name; ?></div>
                        <div class="itemDesc"><?=$item->description;?></div>
                    </div>
                </li>
                   <?php endforeach; ?>
                <?php else:?>
                    <h3 style="padding:15px">Видео еще не добавлено.</h3>
                <?php endif;?>
            </ul>
        </div>
        <div class="paginator">

                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                    'options' => ['class' => false],
                    'prevPageLabel' => false,
                    'nextPageLabel' => false,
                    'activePageCssClass' => 'activeItem',

                ]);?>

        </div>
    </div>
</div>