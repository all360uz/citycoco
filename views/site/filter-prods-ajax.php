<ul>
<?php use yii\helpers\Url;

foreach ($products as $key => $product): ?>

    <?php if ($key == 0 || $key == 7): ?>
        <li class="itemDuble">
            <a
                href="<?= Url::to(['product/product', 'slug' => $product->slug]) ?>">
                <div class="itemImage"><img src="/uploads/<?= $product->img; ?>"/></div>
                <div class="itemData">
                    <div class="itemTitle"><?= $product->name; ?></div>
                    <div class="itemStars">
                        <div class="starsList">
                            <?php if ($product->rating != 0): ?>
                                <?php for ($i = 0; $i < $product->rating; $i++): ?>
                                    <span class="active"></span>
                                <?php endfor; ?>
                            <?php endif; ?>
                            <?php for ($i = 0; $i < 5 - $product->rating; $i++): ?>
                                <span></span>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="itemDesc"><?= substr($product->short_desc, 0, 40); ?></div>
                    <div class="itemPrices">
                        <?php if (!empty($product->discount)): ?>
                            <div class="itemPrice"><?= number_format($product->discount, 0, ' ', ' ') ?>
                                СУМ
                            </div>
                            <div class="itemOldPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                СУМ
                            </div>
                        <?php else: ?>
                            <div class="itemPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                СУМ
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </a></li>
    <?php else: ?>
        <li><a href="<?= Url::to(['product/product', 'slug' => $product->slug]) ?>">
                <div class="itemImage"><img src="/uploads/<?= $product->img; ?>"/></div>
                <div class="itemData">
                    <div class="itemTitle"><?= $product->name; ?></div>
                    <div class="itemStars">
                        <div class="starsList">
                            <?php if ($product->rating != 0): ?>
                                <?php for ($i = 0; $i < $product->rating; $i++): ?>
                                    <span class="active"></span>
                                <?php endfor; ?>
                            <?php endif; ?>
                            <?php for ($i = 0; $i < 5 - $product->rating; $i++): ?>
                                <span></span>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="itemPrices">
                        <?php if (!empty($product->discount)): ?>
                            <div class="itemPrice"><?= number_format($product->discount, 0, ' ', ' ') ?>
                                СУМ
                            </div>
                            <div class="itemOldPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                СУМ
                            </div>
                        <?php else: ?>
                            <div class="itemPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                СУМ
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </a></li>
    <?php endif; ?>
<?php endforeach; ?>
</ul>
