<?php
/**
 * Created by PhpStorm.
 * User: Nazirov
 * Date: 26.03.2018
 * Time: 11:53
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<ul>
    <?php if (!empty($popular_products)): ?>
        <?php foreach($popular_products as $key => $item): ?>
            <?php if($key % 7 == 0 || $key == 0): ?>
                <li class="itemDuble">
                    <a href="<?= Url::to(['product/product', 'slug' => $item->slug])?>">
                        <div class="itemImage">
                            <?= Html::img(Yii::getAlias('@web') . '/uploads/' . $item->img) ?>
                        </div>
                        <div class="itemData">
                            <div class="itemTitle"><?=$item->name; ?></div>
                            <div class="itemStars">
                                <div class="starsList">
                                    <?php if ($item->rating != 0): ?>
                                        <?php for($i = 0; $i < $item->rating; $i++):?>
                                            <span class="active"></span>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    <?php for($i = 0; $i < 5-$item->rating; $i++):?>
                                        <span></span>
                                    <?php endfor; ?>
                                </div>
                            </div>
                            <div class="itemDesc"><?= substr($item->short_desc, 0, 50);?>...</div>
                            <div class="itemPrices">
                                <?php if(!empty($item->discount)):?>
                                    <div class="itemPrice"><?=number_format($item->discount, 0, ' ', ' ')?> СУМ</div>
                                    <div class="itemOldPrice"><?=number_format($item->price, 0, ' ', ' ')?> СУМ</div>
                                <?php else: ?>
                                    <div class="itemPrice"><?=number_format($item->price, 0, ' ', ' ')?> СУМ</div>
                                <?php endif;?>
                            </div>
                        </div>
                    </a>
                </li>
            <?php else: ?>
                <li> <a href="<?= Url::to(['product/product', 'slug' => $item->slug])?>">
                        <div class="itemData">
                            <div class="itemTitle"><?=$item->name; ?></div>
                            <div class="itemStars">
                                <div class="starsList">
                                    <?php if ($item->rating != 0): ?>
                                        <?php for($i = 0; $i < $item->rating; $i++):?>
                                            <span class="active"></span>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    <?php for($i = 0; $i < 5-$item->rating; $i++):?>
                                        <span></span>
                                    <?php endfor; ?>
                                </div>
                            </div>
                            <div class="itemPrices">
                                <?php if(!empty($item->discount)):?>
                                    <div class="itemPrice"><?=number_format($item->discount, 0, ' ', ' ')?> СУМ</div>
                                    <div class="itemOldPrice"><?=number_format($item->price, 0, ' ', ' ')?> СУМ</div>
                                <?php else: ?>
                                    <div class="itemPrice"><?=number_format($item->price, 0, ' ', ' ')?> СУМ</div>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="itemImage">
                            <?= Html::img(Yii::getAlias('@web') . '/uploads/' . $item->img) ?>
                        </div>
                    </a>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</ul>
