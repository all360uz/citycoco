<div class="mainContainer">
    <div class="contactsPage">
        <div class="contactBoxes">
            <?php if(!empty($contacts)): ?>
             <?php foreach($contacts as $item):?>
            <div class="contactBox">
                <div style="background-image: url(/uploads/<?= $item->img;?>);" class="cAva"></div>
                <div class="cName"><?= $item->name;?> <?= $item->sourname;?></div>
                <div class="cPosition"><?= $item->position;?></div>
                <p> <a href="tel:+998946177990"><i><img src="/images/icons/tel.svg"/></i><span><?= $item->phone;?></span></a></p>
                <p> <a href="mailto:sos@info.uz"><i><img src="/images/icons/email.svg"/></i><span><?= $item->email;?></span></a></p>
            </div>
             <?php endforeach;?>
                <?php else: ?>
                <h3 style="margin: 15px">Контакты еще не добавлены</h3>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="mapsBlock">
    <div class="textCenter"><img src='/images/google-maps.png'>
        <?php if(!empty($contact)):?>
            <?= $contact->address; ?>
        <?php endif;?>
    </div>
    <div class="iFrameBlock"> <?php if(!empty($contact)):?>
            <?= $contact->map; ?>
        <?php endif;?></div>
</div>