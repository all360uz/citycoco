<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<div class="mainContainer">
    <div class="contentPadding">
        <div class="checkoutPage">
            <ul class="stepsCheckout">
                <li>01 Вход в систему</li>
                <li class="active">02 доставка</li>
                <li>03 оплата</li>
            </ul>
            <?php  ActiveForm::begin(['action'=> Url::to(['product/pay', 'id' => $user_address->id])]) ?>
                <h2 class="title-1">Адрес доставки</h2>
                <div class="addressBox checkoutBox">
                    <div class="addressColl">
                        <?php if(!empty($user_address)): ?>
                        <p><?= $user_address->place; ?><br> <?= $user_address->address; ?><br> Тел:  +<?= $user_address->phone; ?></p>
                        <div class="addressRL"><a href="<?= Url::to(['product/update-address', 'id' => $user_address->id])?>">Изменить адрес</a>
<!--                            <a href="#">Новый адрес</a>-->
                        </div>
                        <input id="lat" class="hidden" value="<?= $user_address->lat; ?>" />
                        <input id="lng" class="hidden" value="<?= $user_address->lng; ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="mapsColl">

                        <div class="field" id="map2">
                            <script>



                                function initMap( ) {
                                    var lat1 = Number(document.getElementById("lat").value);
                                    var lng1 = Number(document.getElementById("lng").value);
                                    var uluru = {lat: lat1, lng: lng1};
                                    var map = new google.maps.Map(document.getElementById('map2'), {
                                        zoom: 15,
                                        center: uluru

                                    });
                                    var marker = new google.maps.Marker({

                                        position: uluru,
                                        map: map
                                    });
                                }


                            </script>
                            <script async defer
                                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
                            </script>

                        </div>

                    </div>
                </div>
                <h2 class="title-1">Тип доставки</h2>
                <div class="dMethods checkoutBox">

                    <?php foreach ($delivery_type as $item): ?>
                    <div class="dMethodBox" >
                        <label>
                                <input type="radio" value="<?= $item->id ?>" name="d_method" class="styler" checked/>
                                <span class="dMethodTitle"><?= $item->name ?></span>
                                <span class="dMethodPrice">
                                    <?= $item->price == 0 ? 'Бесплатно' : number_format($item->price, 0,' ',' ').' сум'; ?>
                                </span>
                                <span class="dMethodDesc"><?= $item->description ?></span>
                        </label>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="submitCheckout">
                    <button class="btn0 btn1 right">Продолжить</button>
<!--                    <a href="#" class="btnBack left">Продожить покупки</a>-->
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>