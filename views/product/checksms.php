<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<div class="mainContainer">
    <div class="contentPadding">
        <div class="checkoutPage">
            <ul class="stepsCheckout">
                <li class="active">01 Вход в систему</li>
                <li>02 доставка</li>
                <li>03 оплата</li>
            </ul>
            <h1 class="title-1">Вход в систему</h1>
           <?php ActiveForm::begin([
                   'action' => Url::to(['/product/checksms', 'id' => $model->id])
           ]) ?>
                <div class="formControl required">

                        <div class="alert-error-sms">
                            <p></p>
                        </div>

                    <label>Введите номер телефона</label>
                    <div class="field fAdd"><span class="fAddCh">+998</span>
                        <div class="overflowed">
                            <input type="tel" required="required" value="<?= substr($model->phone, 3,10);?>" disabled/>
                        </div>
                    </div>
                    <div class="fieldText"> <span>Мы отправили Вам код через СМС</span>
                        <div class="timer"><span class="minute">00</span><span>:</span><span class="secound">59</span></div>
                    </div>
                </div>
                <div id="codeField" class="formControl required">
                    <label>Введите код</label>
                    <div class="field">
                        <input type="text" name="sms" required="required" maxlength="6" minlength="6"/>
                    </div>
                </div>
                <div class="submitCheckout">
                    <button class="btn0 btn1 right">Продолжить</button><a href="<?= Url::to(['/product/log']) ?>" class="btnBack left hidden">Назад</a>
                </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>