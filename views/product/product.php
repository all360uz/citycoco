<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<div class="mainContainer">
    <div class="productPage productPagePadding">
<!--            Product Images      -->
        <?php if(!empty($product)):?>
        <div class="productImages">
            <div class="minImages">
                <ul class="MagicScroll">
                        <li><a data-image="/uploads/<?= $product->img; ?>" data-zoom-id="Zoom-1" href="/uploads/<?= $product->img; ?>"><img src="/uploads/<?= $product->img; ?>"/></a></li>
                        <?php foreach ($product->productImages as $item): ?>
                    <li><a data-image="/uploads/<?= $item->img; ?>" data-zoom-id="Zoom-1" href="/uploads/<?= $item->img; ?>"><img src="/uploads/<?= $item->img; ?>"/></a></li>
                        <?php endforeach;?>
                </ul>
            </div>
            <div class="bigImage"><a data-options="variableZoom: true" href="/uploads/<?= $product->img; ?>" id="Zoom-1" class="MagicZoom"><img src="/uploads/<?= $product->img; ?>"/></a></div>
        </div>
        <?php endif; ?>
<!--            END Product Images      -->
<!--        Product Info                -->
        <div class="productData">
            <h1><?= $product->name; ?></h1>
            <div class="itemStars">
                <div class="starsList starsListB">
                    <?php if ($product->rating != 0): ?>
                        <?php for($i = 0; $i < $product->rating; $i++):?>
                            <span class="active"></span>
                        <?php endfor; ?>
                    <?php endif; ?>
                    <?php for($i = 0; $i < 5-$product->rating; $i++):?>
                        <span></span>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="itemDesc"><?= $product->short_desc; ?></div>
            <div class="pColor">
                <?php foreach ($product->productsAddFilters as $item): ?>
                <a class="addfilter-a"><span style="background: <?= $item->addFilter->code; ?>"></span></a>
                <?php endforeach; ?>
            </div>
            <div class="itemPrices">
                <?php if(!empty($product->discount)):?>
                    <div class="itemPrice"><?=number_format($product->discount, 0, ' ', ' ')?> СУМ</div>
                    <div class="itemOldPrice"><?=number_format($product->price, 0, ' ', ' ')?> СУМ</div>
                <?php else: ?>
                    <div class="itemPrice"><?=number_format($product->price, 0, ' ', ' ')?> СУМ</div>
                <?php endif;?>
            </div>
            <div class="productButtons">
                <div class="num">
                    <button class="nMinus disabled">-</button>
                    <input type="text" value="1" class="val" id="qty"/>
                    <button class="nPlus">+</button>
                </div><a href="<?= \yii\helpers\Url::to(['product/cartadd', 'id' => $product->id])?>" class="pBtn1 add-to-cart" data-id="<?=$product->id; ?>">В корзину</a>
            </div>
        </div>
    </div>
<!--    END Product info        -->
</div>
<!--Product Main Params-->
<div class="productParams">
    <div class="mainContainer">
        <div class="productParamsCont productPagePadding">
            <ul>
                <?php foreach ($product->productsProductParams as $item): ?>
                <li>
                    <div class="paramTitle"><?= $item->productParams->name; ?></div>
                    <div class="paramValue"><?= $item->value; ?></div>
                </li>
                <?php endforeach; ?>
                <li><a class="link">Подробно о товаре</a></li>
            </ul>
        </div>
    </div>
</div>
<!--END Product Main Params-->
<!--Product Content/Params/Reviews/re-->
<div class="mainContainer">
    <div class="productTabNav">
        <nav>
            <ul>
                <li class="activeItem"><a data-href="1">Описание</a></li>
                <li><a data-href="2">Характеристики</a></li>
                <li><a data-href="3">Отзывы (<?= $count_reviews;?>)</a></li>
                <li><a data-href="4">Аксесуары и запчасти</a></li>
            </ul>
        </nav>
    </div>
    <div class="productTabBody productPagePadding">
        <div class="pTabBody pTabBody1">
            <div class="postContent">
                <p><?= $product->content; ?></p>
            </div>
        </div>
        <div class="pTabBody pTabBody2">
            <div class="table1">
                <div class="table1Title">Параметры</div>
                <table>
                    <?php foreach ($product->productsProductParams as $item): ?>
                    <tr>
                        <td><?= $item->productParams->name; ?></td>
                        <td><?= $item->value; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <div class="table1">
                <div class="table1Title">Индивидуальные параметры</div>
                <table>
                    <?php foreach ($product->productSelfParams as $items): ?>
                    <tr>
                        <td><?= $items->name; ?></td>
                        <td><?= $items->value; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
        <div class="pTabBody pTabBody3">
            <div class="reviewsList">
                <?php if(!empty($reviews)): ?>
                    <?php foreach ($reviews as $item): ?>
                    <div class="review">
                    <div class="rStars">
                        <div class="starsList starsListB">
                            <?php if ($item->rate != 0): ?>
                                <?php for($i = 0; $i < $item->rate; $i++):?>
                                    <span class="active"></span>
                                <?php endfor; ?>
                            <?php endif; ?>
                            <?php for($i = 0; $i < 5-$item->rate; $i++):?>
                                <span></span>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="rUserName"><?= $item->name; ?></div>
                    <div class="rText"><?= $item->review; ?></div>
                    <div class="rDate"><?= date('M d, Y', $item->created_at); ?></div>
                </div>
                    <?php endforeach;?>
                    <?php else: ?>
                    <h3 style="margin: 15px">К этому продукту отзыв еще не добавлено</h3>
                <?php endif; ?>
            </div>
            <div class="paginator">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                    'options' => ['class' => false],
                    'prevPageLabel' => false,
                    'nextPageLabel' => false,
                    'activePageCssClass' => 'activeItem',

                ]);?>
            </div>
            <?php ActiveForm::begin([
                    'action' => '/product/add-review',
                    'options' => [
                            'class' => 'newReviewForm'
                    ]]
            ) ?>
                <input type="text" name="id" value="<?= $product->id ?>" style="display: none;"/>
                <h3>Написать отзыв</h3>
                <div class="formControl required">
                    <label>Ваше имя</label>
                    <div class="field">
                        <input type="text" name="name" required="required"/>
                    </div>
                </div>
                <div class="formControl required">
                    <label>Ваш отзыв</label>
                    <div class="field">
                        <textarea rows="2" name="review" required="required"></textarea>
                    </div>
                </div>
                <div class="rating">
                    <label>Рейтинг</label>
                    <div class="new_stars">
                        <input type="hidden" value="0" name="rate"/>
                        <ul>
                            <li title="1"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="15" viewBox="0 0 16 15"><g transform="translate(-716 -3479)"><path fill="#a2a2a2" d="M723.49 3491l-4.63 2.47.89-5.23-3.75-3.71 5.18-.77 2.31-4.76 2.32 4.76 5.18.77-3.75 3.71.88 5.23z"/></g></svg></li>
                            <li title="2"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="15" viewBox="0 0 16 15"><g transform="translate(-716 -3479)"><path fill="#a2a2a2" d="M723.49 3491l-4.63 2.47.89-5.23-3.75-3.71 5.18-.77 2.31-4.76 2.32 4.76 5.18.77-3.75 3.71.88 5.23z"/></g></svg></li>
                            <li title="3"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="15" viewBox="0 0 16 15"><g transform="translate(-716 -3479)"><path fill="#a2a2a2" d="M723.49 3491l-4.63 2.47.89-5.23-3.75-3.71 5.18-.77 2.31-4.76 2.32 4.76 5.18.77-3.75 3.71.88 5.23z"/></g></svg></li>
                            <li title="4"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="15" viewBox="0 0 16 15"><g transform="translate(-716 -3479)"><path fill="#a2a2a2" d="M723.49 3491l-4.63 2.47.89-5.23-3.75-3.71 5.18-.77 2.31-4.76 2.32 4.76 5.18.77-3.75 3.71.88 5.23z"/></g></svg></li>
                            <li title="5"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="15" viewBox="0 0 16 15"><g transform="translate(-716 -3479)"><path fill="#a2a2a2" d="M723.49 3491l-4.63 2.47.89-5.23-3.75-3.71 5.18-.77 2.31-4.76 2.32 4.76 5.18.77-3.75 3.71.88 5.23z"/></g></svg></li>
                        </ul>
                    </div>
                </div>
                <div class="formSubmit textCenter">
                    <button type="submit" class="btn0 btn1">Продолжить</button>
                </div>

            <?php ActiveForm::end();?>
        </div>
        <div class="pTabBody pTabBody4">
            <div class="addPList">
                <div class="productsList product_zapchast">
                    <ul>
                        <?php if($product->productsRelations): ?>
                            <?php foreach ($product->productsRelations as $item): ?>
                        <li>
                            <a href="<?= Url::to(['/product/product', 'slug' =>$item->relatedProduct->slug])?>">
                                <div class="itemData">
                                    <div class="itemTitle"><?= $item->relatedProduct->name; ?></div>
                                    <div class="itemStars">
                                        <div class="starsList">

                                            <?php if ($item->relatedProduct->rating != 0): ?>
                                                <?php for($i = 0; $i < $item->relatedProduct->rating; $i++):?>
                                                    <span class="active"></span>
                                                <?php endfor; ?>
                                            <?php endif; ?>
                                            <?php for($i = 0; $i < 5-$item->relatedProduct->rating; $i++):?>
                                                <span></span>
                                            <?php endfor; ?>

                                        </div>
                                        </div>
                                    <div class="itemPrices">
                                        <?php if(!empty($item->discount)):?>
                                            <div class="itemPrice"><?=number_format($item->relatedProduct->discount, 0, ' ', ' ')?> СУМ</div>
                                            <div class="itemOldPrice"><?=number_format($item->relatedProduct->price, 0, ' ', ' ')?> СУМ</div>
                                        <?php else: ?>
                                            <div class="itemPrice"><?=number_format($item->relatedProduct->price, 0, ' ', ' ')?> СУМ</div>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="itemImage"><img src="/uploads/<?= $item->relatedProduct->img;?>"/>
                                </div>
                            </a>
                        </li>
                                <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END Product Content/Params/Reviews/re-->