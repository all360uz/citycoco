<?php
use yii\widgets\ActiveForm;
/**
 * Created by PhpStorm.
 * User: Nazirov
 * Date: 27.03.2018
 * Time: 11:46
 */
?>
<div class="mainContainer">
    <div class="contentPadding">
        <div class="checkoutPage">
            <ul class="stepsCheckout">
                <li class="active">01 Вход в систему</li>
                <li>02 доставка</li>
                <li>03 оплата</li>
            </ul>
            <h1 class="title-1">Вход в систему</h1>
            <?php ActiveForm::begin(['action' => \yii\helpers\Url::to(['/product/login'])]) ?>
                <div class="formControl required">

                        <div class="alert-error-sms">
                           <p></p>
                        </div>

                    <label>Введите номер телефона</label>
                    <div class="field fAdd"><span class="fAddCh">+998</span>
                        <div class="overflowed">
                            <input type="tel" name="phone" pattern="[9]{1}[0,1,3,4,5,7,8,9]{1}[1-9]{1}[0-9]{6}" required="required" placeholder="901234567" maxlength="9" minlength="9" />
                        </div>
                    </div>
                </div>
                <div class="submitCheckout">
                    <button class="btn0 btn1 right" >Продолжить</button><a href="/" class="btnBack left">Продожить покупки</a>
                </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
