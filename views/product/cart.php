<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="mainContainer">
    <div class="contentPadding">
        <div class="cartPage">
            <div class="cartTable">
                <table>
                    <thead>
                    <tr>
                        <td colspan="2">Товар</td>
                        <td>Количество </td>
                        <td>Цена</td>
                        <td> </td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($session['cart'])):?>
                        <?php foreach ($session['cart'] as $id => $item):?>
                         <tr>
                             <td>
                                 <div class="cartPImage"><img src="/uploads/<?= $item['img']; ?>" alt=""/></div>
                             </td>
                             <td>
                                 <div class="cartPData">
                                     <div class="cartPTitle"><?= $item['name']; ?></div>
                                     <?php if(!empty($item['discount'])):?>
                                         <div class="cartPPrice"><?=number_format($item['discount'], 0, ' ', ' ')?> сум</div>
                                     <?php else: ?>
                                         <div class="cartPPrice"><?=number_format($item['price'], 0, ' ', ' ')?> сум</div>
                                     <?php endif;?>
                                 </div>
                             </td>
                             <td>
                                 <div class="num cartNum">
                                     <button class="nMinus disabled minus-to-cart-prod" data-id="<?= $id ?>">-</button>
                                     <input type="text" value="<?= $item['qty']; ?>" class="val"/>
                                     <button class="nPlus add-to-cart-prod" data-id="<?= $id ?>">+</button>
                                 </div>
                             </td>
                             <td>
                                 <?php if(!empty($item['discount'])):?>
                                 <div class="cartRowPrice"><?=number_format($item['discount']*$item['qty'], 0, ' ', ' ')?> сум</div>
                                 <?php else: ?>
                                 <div class="cartRowPrice"><?=number_format($item['price']*$item['qty'], 0, ' ', ' ')?> сум</div>
                                 <?php endif;?>
                             </td>
                             <td><a href="#" class="closeBtn del-item" data-id="<?= $id ?>"></a></td>
                         </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <h4 style="margin: 15px">Вы не добавили не один продукт в корзину.</h4>
                    <?php endif; ?>

                    </tbody>

                </table>

            </div>
            <div class="cartTableTotals">
                <?php  if($delivery->price == 0): ?>
                 <p>Доставка: Бесплатно</p>
                <?php else: ?>
                    <p>Доставка: <?=number_format($delivery->price, 0, ' ', ' ')?> сум</p>
                <?php endif; ?>
                <p>Итого:
                    <?php if(!empty($session['cart.sum'])): $sum =  $_SESSION['cart.sum'] + $delivery->price;  else: $sum = '0'; endif; ?>
                    <span><?=number_format($sum, 0, ' ', ' ')?></span>
                    сум</p>
            </div>
            <div class="textCenter">
                <!-- button.btn0.btn1 оформить заказ--><a href="<?= Url::to(['product/log'])?>" class="btn0 btn1 cartSubmit uppercase">оформить заказ</a>
            </div>
        </div>
    </div>
</div>