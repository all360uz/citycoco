<?php
/**
 * Created by PhpStorm.
 * User: Nazirov
 * Date: 29.03.2018
 * Time: 14:40
 */
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<div class="mainContainer">
    <div class="contentPadding">
        <div class="checkoutPage">
            <ul class="stepsCheckout">
                <li>01 Вход в систему</li>
                <li>02 доставка</li>
                <li class="active">03 оплата</li>
            </ul>
           <?php ActiveForm::begin(['action' => Url::to(['/product/payment', 'id' => $orders->id])])?>
                <h2 class="title-1">Оплата</h2>
                <div class="payMethods checkoutBox">
                    <?php foreach ($payment_types as $item): ?>
                    <div class="dMethodBox">
                        <label>
                            <input type="radio" name="payment_type" value="<?=$item->id; ?>" class="styler" required="required" checked/>
                            <span class="dMethodTitle"><?= $item->name; ?></span>
                            <span class="dMethodPrice">
                                <img src="/uploads/<?= $item->logo; ?>" alt=""/>
                            </span>
                            <span class="dMethodDesc"><?= $item->description; ?></span>
                        </label>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="cartTableTotals">
                    <p>Доставка:
                        <?php $price = $orders->deliverytType->price; ?>
                        <?= $price == 0 ? 'Бесплатно' : $price; ?>
                    </p>
                    <p>Итого: <span><?= number_format($price+$orders->amount,'0', ' ', ' ');  ?> </span> сум</p>
                </div>
                <div class="submitCheckout">
                    <button class="btn0 btn2 right">подтвердить оплату</button>
                    <a href="<?= Url::to(['/product/update-address', 'id' => $user_address->id]) ?>" class="btnBack left">Назад</a>
                </div>
           <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
