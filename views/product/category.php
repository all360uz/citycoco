<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;

?>
<div class="mainContainer">
    <div class="contentPadding">
        <div class="pageRow">
            <div id="catListBar" class="pageSlideBar">
                <div class="catList">
                    <div class="sideBarTitle">Категории</div>
                    <ul class="sidebarList">

                        <?php if (!empty($categories)): ?>
                            <?php foreach ($categories as $item): ?>
                                <li class="<?= $id == $item->id ? 'activeItem' : null ?>">
                                    <a href="<?= \yii\helpers\Url::to(['/product/category', 'id' => $item->id]) ?>">
                                        <?= $item->name; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <?php if (!empty($category->related_product_id)): ?>
                <div class="pageContent">
                    <div class="categoryFirstItem">
                        <div class="productData">
                            <h1><?= $category->relatedProduct->name ?></h1>
                            <div class="itemStars">
                                <div class="starsList starsListB">
                                    <?php if ($category->relatedProduct->rating != 0): ?>
                                        <?php for ($i = 0; $i < $category->relatedProduct->rating; $i++): ?>
                                            <span class="active"></span>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    <?php for ($i = 0; $i < 5 - $category->relatedProduct->rating; $i++): ?>
                                        <span></span>
                                    <?php endfor; ?>
                                </div>
                            </div>
                            <div class="itemDesc"><?= $category->relatedProduct->short_desc ?></div>
                            <div class="itemPrices">
                                <?php if (!empty($category->relatedProduct->discount)): ?>
                                    <div class="itemPrice"><?= number_format($category->relatedProduct->discount, 0, ' ', ' ') ?>
                                        СУМ
                                    </div><br>
                                    <div class="itemOldPrice"><?= number_format($category->relatedProduct->price, 0, ' ', ' ') ?>
                                        СУМ
                                    </div>
                                <?php else: ?>
                                    <div class="itemPrice"><?= number_format($category->relatedProduct->price, 0, ' ', ' ') ?>
                                        СУМ
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="productButtons"><a
                                        href="<?= Url::to(['/product/product', 'slug' => $category->relatedProduct->slug]) ?> "
                                        class="btn0 btnRadius btn1">Подробнее</a></div>
                        </div>
                        <div class="cfProductImage"><img src="/uploads/<?= $category->relatedProduct->img; ?>" alt=""/>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <?php if (!empty($related_product)): ?>
                    <div class="pageContent">
                        <div class="categoryFirstItem">
                            <div class="productData">
                                <h1><?= $related_product->name ?></h1>
                                <div class="itemStars">
                                    <div class="starsList starsListB">
                                        <?php if ($related_product->rating != 0): ?>
                                            <?php for ($i = 0; $i < $related_product->rating; $i++): ?>
                                                <span class="active"></span>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                        <?php for ($i = 0; $i < 5 - $related_product->rating; $i++): ?>
                                            <span></span>
                                        <?php endfor; ?>
                                    </div>
                                </div>
                                <div class="itemDesc"><?= $related_product->short_desc ?></div>
                                <div class="itemPrices">
                                    <?php if (!empty($related_product->discount)): ?>
                                        <div class="itemPrice"><?= number_format($related_product->discount, 0, ' ', ' ') ?>
                                            СУМ
                                        </div><br>
                                        <div class="itemOldPrice"><?= number_format($related_product->price, 0, ' ', ' ') ?>
                                            СУМ
                                        </div>
                                    <?php else: ?>
                                        <div class="itemPrice"><?= number_format($related_product->price, 0, ' ', ' ') ?>
                                            СУМ
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="productButtons"><a
                                            href="<?= Url::to(['/product/product', 'slug' => $related_product->slug]) ?> "
                                            class="btn0 btnRadius btn1">Подробнее</a></div>
                            </div>
                            <div class="cfProductImage"><img src="/uploads/<?= $related_product->img; ?>" alt=""/></div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="rowHead">
            <button id="filterBtn" class="btn0 btn1 btnRadius">Фильтр</button>
            <div class="sectionSort sort-category">
                <ul>
                    <?php if (!empty($sort)): ?>
                        <?php foreach ($sort as $item): ?>
                            <li>
                                <a href="javascript:void(0)" data-id="<?= $category->id ?>" data-sort="<?= $item->sort_data ?>" >
                                    <span><?= $item->name; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </ul>
                <div class="sortMore"><a class="sortMoreBtn"><img src="/images/icons/more.svg"/></a>
                    <ul>
                        <?php if (!empty($sort)): ?>
                            <?php foreach ($sort as $item): ?>
                                <li class="activeItem"><a href="#"><span><?= $item->name; ?></span></a></li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="pageRow">
            <div id="filter" class="pageSlideBar">
                <div class="catList">
                    <div class="sideBarTitle">Фильтр</div>
                    <div class="filters">
                        <form id="filter-coco" >
                            <input type="hidden" name="id" value="<?= Yii::$app->request->get('id') ?>">
                            <div class="filterBox">
                                <div class="filterTitle">Наши предложения</div>
                                <div class="filterCont">
                                    <ul class="filterCheckList">
                                        <?php if (!empty($main_filter)): ?>
                                            <?php foreach ($main_filter as $item): ?>
                                                <li data-cat_id="<?=$category->id; ?>" >
                                                    <label>
                                                        <input type="checkbox" name="our_recommend[]" <?= in_array($item->id, $recommend) ? 'checked' : null ?> value="<?= $item->id ?>" class="styler" data-cat_id="<?=$category->id; ?>"/>
                                                        <span><?= $item->name; ?></span>
                                                    </label>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="filterBox">
                                <div class="filterTitle">Производитель</div>
                                <div class="filterCont">
                                    <ul class="filterCheckList">
                                        <?php foreach ($category->brandsCategories as $item): ?>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="brands[]" <?= in_array($item->brands_id, $brands) ? 'checked' : null ?> value="<?= $item->brands_id ?>" class="styler"/>
                                                    <span><?= $item->brands->name; ?></span>
                                                </label>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            <?php if (!empty($add_filter)):; ?>
                                <?php foreach ($add_filter as $item): ?>
                                    <div class="filterBox">
                                        <?php if ($item->parent_id == NULL): ?>
                                            <div class="filterTitle"><?= $item->name; ?></div>

                                            <div class="filterCont">
                                                <div class="row5">

                                                    <div class="colp5-6">
                                                        <ul class="filterCheckList">
                                                            <?php foreach ($item->parents as $key => $items): ?>
                                                            <li>
                                                                <label>
                                                                    <input type="checkbox" name="add_filter[]" <?= in_array($items->id, $custom_filter) ? 'checked' : null ?> value="<?= $items->id ?>" class="styler"/>
                                                                    <span><?= $items->name; ?></span>
                                                                </label>
                                                            </li>
                                                            <?php if ($key == 4): ?>
                                                        </ul>
                                                    </div>
                                                    <div class="colp5-6">
                                                        <ul class="filterCheckList">
                                                            <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?= Html::submitButton('Submit', $options = ['style' => 'display:none']); ?>
                        </form>
                        <div class="filterTitle notArrow">Выбранный фильтр</div>
                        <ul class="selectedList">
                            <?php foreach ($main_filter as $item_r): ?>
                                <?php if (in_array($item_r->id, $recommend)): ?>
                                    <li data-name="our_recommend[]" data-value="<?= $item_r->id ?>">
                                        <a href="#" >
                                            <span><?= $item_r->name ?></span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php foreach ($category->brandsCategories as $item_b): ?>
                                <?php if (in_array($item_b->brands_id, $brands)): ?>
                                    <li  data-name="brands[]" data-value="<?= $item_b->brands_id; ?>">
                                        <a href="#">
                                            <span><?= $item_b->brands->name; ?></span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php foreach ($add_filter as $item_c): ?>
                                <?php if (in_array($item_c->id, $custom_filter)): ?>
                                    <li data-name="add_filter[]" data-value="<?= $item_c->id ?>">
                                        <a href="#" >
                                            <span><?= $item_c->name; ?></span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                        <a href="#" class="redLink removelallfilter">Сбросить фильтр </a>
                    </div>
                </div>
            </div>
            <div class="pageContent">
                <div class="productsList">
                    <ul>
                        <?php if ($products) : ?>
                            <?php foreach ($products as $product): ?>
                                <li>
                                    <a href="<?= Url::to(['product/product', 'slug' => $product->slug]) ?>">
                                        <div class="itemImage"><img src="/uploads/<?= $product->img; ?>"/></div>
                                        <div class="itemData">
                                            <div class="itemTitle"><?= $product->name; ?></div>
                                            <div class="itemStars">
                                                <div class="starsList">
                                                    <?php if ($product->rating != 0): ?>
                                                        <?php for ($i = 0; $i < $product->rating; $i++): ?>
                                                            <span class="active"></span>
                                                        <?php endfor; ?>
                                                    <?php endif; ?>
                                                    <?php for ($i = 0; $i < 5 - $product->rating; $i++): ?>
                                                        <span></span>
                                                    <?php endfor; ?>
                                                </div>
                                            </div>
                                            <div class="itemPrices">
                                                <?php if (!empty($product->discount)): ?>
                                                    <div class="itemPrice"><?= number_format($product->discount, 0, ' ', ' ') ?>
                                                        СУМ
                                                    </div>
                                                    <div class="itemOldPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                                        СУМ
                                                    </div>
                                                <?php else: ?>
                                                    <div class="itemPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                                        СУМ
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <h3 style="margin: 15px">Продук еще не добавлен</h3>
                        <?php endif; ?>
                    </ul>
                </div>


                <div class="paginator">
                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $pages,
                        'options' => ['class' => false],
                        'prevPageLabel' => false,
                        'nextPageLabel' => false,
                        'activePageCssClass' => 'activeItem',

                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>