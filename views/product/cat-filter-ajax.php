<?php
/**
 * Created by PhpStorm.
 * User: Nazirov
 * Date: 27.03.2018
 * Time: 15:55
 */
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php if ($products) : ?>
    <?php foreach ($products as $product): ?>
        <li>
            <a href="<?= Url::to(['product/product', 'slug' => $product->slug]) ?>">
                <div class="itemImage"><img src="/uploads/<?= $product->img; ?>"/></div>
                <div class="itemData">
                    <div class="itemTitle"><?= $product->name; ?></div>
                    <div class="itemStars">
                        <div class="starsList">
                            <?php if ($product->rating != 0): ?>
                                <?php for ($i = 0; $i < $product->rating; $i++): ?>
                                    <span class="active"></span>
                                <?php endfor; ?>
                            <?php endif; ?>
                            <?php for ($i = 0; $i < 5 - $product->rating; $i++): ?>
                                <span></span>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="itemPrices">
                        <?php if (!empty($product->discount)): ?>
                            <div class="itemPrice"><?= number_format($product->discount, 0, ' ', ' ') ?>
                                СУМ
                            </div>
                            <div class="itemOldPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                СУМ
                            </div>
                        <?php else: ?>
                            <div class="itemPrice"><?= number_format($product->price, 0, ' ', ' ') ?>
                                СУМ
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </a>
        </li>
    <?php endforeach; ?>
<?php else: ?>
    <h3 style="margin: 15px">Продукты еще не добавлены</h3>
<?php endif; ?>