<?php
/**
 * Created by PhpStorm.
 * User: Nazirov
 * Date: 26.03.2018
 * Time: 14:30
 */?>

<div class="mainContainer">
    <div class="searchPage">
        <div class="searchForm">
            <form action="/product/search" method="get">
                <input id="searchField2" name="q" type="text" placeholder="<?= empty($q) ? '' : $q ?>"/><span data-field="#searchField2" class="clearField"><img src="images/icons/close2.svg"/></span>
            </form>
        </div>
        <div class="resultSearch">
            <div class="productsList">
                <ul>
                    <?php if (!empty($products)):?>
                        <?php foreach ($products as $item): ?>
                    <li><a href="<?=\yii\helpers\Url::to(['/product/product', 'slug' => $item->slug])?>">
                            <div class="itemData">
                                <div class="itemTitle"><?= $item->name; ?></div>
                                <div class="itemStars">
                                    <div class="starsList">
                                        <?php if ($item->rating != 0): ?>
                                            <?php for($i = 0; $i < $item->rating; $i++):?>
                                                <span class="active"></span>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                        <?php for($i = 0; $i < 5-$item->rating; $i++):?>
                                            <span></span>
                                        <?php endfor; ?>
                                    </div>
                                </div>
                                <div class="itemPrices">
                                    <?php if(!empty($item->discount)):?>
                                        <div class="itemPrice"><?=number_format($item->discount, 0, ' ', ' ')?> СУМ</div>
                                        <div class="itemOldPrice"><?=number_format($item->price, 0, ' ', ' ')?> СУМ</div>
                                    <?php else: ?>
                                        <div class="itemPrice"><?=number_format($item->price, 0, ' ', ' ')?> СУМ</div>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="itemImage"><img src="/uploads/<?= $item->img; ?>"/></div></a></li>
                            <?php endforeach; ?>
                    <?php else: ?>
                        <h3 style="margin: 15px">По вашему запросу ничего не найдено.</h3>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </div>
</div>
