<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div class="mainContainer">
    <div class="contentPadding">
        <div class="checkoutPage">
            <ul class="stepsCheckout">
                <li>01 Вход в систему</li>
                <li class="active">02 доставка</li>
                <li>03 оплата</li>
            </ul>
            <?php ActiveForm::begin(['action' => Url::to(['/product/address', 'id' => $model->id])]) ?>

                <h2 class="title-1">Адрес доставки</h2>
                <div class="formControl">
                    <label>Область/Город</label>
                    <div class="select">
                        <select name="place" data-placeholder="--Выберите область--" class="styler">
                            <option></option>
                            <option value="Город Ташкент">Город Ташкент</option>
                            <option value="Ташкентская область">Ташкентская область</option>
                            <option value="Андижанская область">Андижанская область</option>
                            <option value="Бухарская область">Бухарская область</option>
                            <option value="Джизакская область">Джизакская область</option>
                            <option value="Кашкадарьинская область">Кашкадарьинская область</option>
                            <option value="Навоийская область">Навоийская область</option>
                            <option value="Наманганская область">Наманганская область</option>
                            <option value="Самаркандская область">Самаркандская область</option>
                            <option value="Сурхандарьинская область">Сурхандарьинская область</option>
                            <option value="Сырдарьинская область">Сырдарьинская область</option>
                            <option value="Ташкентская область">Ташкентская область</option>
                            <option value="Ферганская область">Ферганская область</option>
                            <option value="Хорезмская область">Хорезмская область</option>
                            <option value="Республика Каракалпакстан">Республика Каракалпакстан</option>

                        </select>
                    </div>
                </div>
                <div class="formControl">
                    <label>Адрес (Улица/Дом/Квартира, офис)</label>
                    <div class="field">
                        <input type="text" name="address"/>
                    </div>
                </div>
                <div class="formControl">
                    <label>Выберите место для доставки на карте</label>
                    <div id="floating-panel">
                        <input onclick="deleteMarkers();" type=button value="Удалить выбраноое место">
                    </div>
                    <div class="field" id="map">
                            <script>
                                var map;
                                var markers = [];
                                var i = 1;
                                var lat;
                                var lng;
                                function initMap() {
                                    var haightAshbury = {lat: 41.306688, lng:  69.281285};


                                    map = new google.maps.Map(document.getElementById('map'), {
                                        zoom: 14,
                                        center: haightAshbury,
                                        mapTypeId: 'terrain'
                                    });

                                    // This event listener will call addMarker() when the map is clicked.
                                    map.addListener('click', function(event) {

                                        if( i == 1){addMarker(event.latLng);  i=2;
                                        }
                                    });

                                }

                                // Adds a marker to the map and push to the array.
                                function addMarker(location) {

                                    var marker = new google.maps.Marker({
                                        position: location,
                                        map: map
                                    });
                                    markers.push(marker);
                                    console.log(marker.getPosition().lat());
                                    console.log(marker.getPosition().lng());
                                    $('#lat').val(marker.getPosition().lat());
                                    $('#lng').val(marker.getPosition().lng());

                                }



                                // Sets the map on all markers in the array.
                                function setMapOnAll(map) {
                                    for (var i = 0; i < markers.length; i++) {
                                        markers[i].setMap(map);
                                    }

                                }

                                // Removes the markers from the map, but keeps them in the array.
                                function clearMarkers() {
                                    setMapOnAll(null);
                                }

                                // Shows any markers currently in the array.
                                function showMarkers() {
                                    setMapOnAll(map);
                                }

                                // Deletes all markers in the array by removing references to them.
                                function deleteMarkers() {
                                    clearMarkers();
                                     i = 1;
                                    $('#lat').val('');
                                    $('#lng').val('');
                                    markers = [];
                                }


                            </script>
                            <script async defer
                                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
                            </script>

                    </div>
                </div>
                <input type="text" name="lat" id="lat" value="" class="hidden" />
                <input type="text" name="lng" id="lng" value="" class="hidden" />
                <div class="submitCheckout">
                    <button class="btn0 btn1 right">Продолжить</button>
<!--                    <a href="#" class="btnBack left">Назад</a>-->
                </div>
           <?php ActiveForm::end();?>
        </div>
    </div>
</div>