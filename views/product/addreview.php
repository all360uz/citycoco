<div class="mainContainer">
    <div class="contactsPage">
        <div class="nPage">
            <div class="nPageBox">
                <div class="check-ico"><img src="/images/icons/check.svg" alt=""/></div>
                <h1>Спасибо за отзыв!</h1>
                <p>Ваш отзыв добавлен!</p>
            </div><a href="<?= \yii\helpers\Url::to(['product/product/', 'slug' => $slug])?>" class="btn0 btn1 uppercase">Назад к продукту</a>
           <a href="/" class="btn0 btn1 uppercase" style="margin-top: 15px">На главную</a>
        </div>
    </div>
</div>