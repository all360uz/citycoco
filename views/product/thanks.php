<?php
/**
 * Created by PhpStorm.
 * User: Nazirov
 * Date: 29.03.2018
 * Time: 16:25
 */
?>
<div class="mainContainer">
    <div class="contactsPage">
        <div class="nPage">
            <div class="nPageBox">
                <div class="check-ico"><img src="/images/icons/check.svg" alt=""/></div>
                <h1>Спасибо!</h1>
                <p>Оплата прошла успешно</p>
            </div><a href="/" class="btn0 btn1 uppercase">На главную</a>
        </div>
    </div>
</div>
