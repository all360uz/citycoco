<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\Social;
use app\models\Contacts;

AppAsset::register($this);
    $social = Social::find()->all();
    $contacts = Contacts::find()->where(['id' => 1])->One();
    $category = \app\models\Categories::find()->all();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="
<?= Yii::$app->controller->action->id == 'cart' ? 'whiteBody' : null ?>
<?= Yii::$app->controller->action->id == 'log' ? 'whiteBody' : null ?>
<?= Yii::$app->controller->action->id == 'login' ? 'whiteBody' : null ?>
<?= Yii::$app->controller->action->id == 'checksms' ? 'whiteBody' : null ?>
<?= Yii::$app->controller->action->id == 'address' ? 'whiteBody' : null ?>
<?= Yii::$app->controller->action->id == 'pay' ? 'whiteBody' : null ?>
<?= Yii::$app->controller->action->id == 'update-address' ? 'whiteBody' : null ?>
<?= Yii::$app->controller->action->id == 'address-changed' ? 'whiteBody' : null ?>
">
<?php $this->beginBody() ?>
<div id="minBody">
    <!-- start #mainWrap-->
    <div id="mainWrap">
        <!-- start #header-->
        <div id="header" class="<?= Yii::$app->controller->action->id == 'index' ? 'mainPageHeader' : null ?>">
            <div id="mobileNavBar">
                <div class="mobileNavBarWrapInner">
                    <div class="headerButtons">
                        <a href="#" class="searchBtn"> <img src="/images/icons/search-ico.svg"/></a>
                        <a href="<?= Url::to(['product/cart'])?>">
                            <img src="/images/icons/cart-ico.svg"/>
                            <span class="count"><?= isset($_SESSION['cart']) ? count($_SESSION['cart']) : '0'?></span>
                        </a>
                    </div>
                    <div class="sn">
                        <?php if(!empty($social)):?>
                            <?php foreach ($social as $item): ?>
                                <a href="<?= $item->url ?>"><?= Html::img(Yii::getAlias('@web').'/uploads/'.$item->icon)?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <div class="clear"></div>
                    <nav class="headerMenu">
                        <ul>
                            <li class="<?= Yii::$app->controller->action->id == 'index' ? 'activeItem' : null ?>"><a href="/">Главная</a></li>
                            <li><a href="<?= Url::to(['/site/video'])?>">Видео</a></li>
                            <li><a href="<?= Url::to(['/site/service'])?>">Услуги</a></li>
                            <li><a href="<?= Url::to(['/site/contacts'])?>">Контакты</a></li>
                            <li><a href="<?= Url::to(['/site/testdrive'])?>">Тестдрайв</a></li>
                        </ul>
                    </nav>
                    <nav class="mainBlockBavBar">
                        <ul>
                            <?php if(!empty($category)):?>
                            <?php foreach ($category as $items): ?>
                                <li><a href="<?=Url::to(['/product/category', 'id' => $items->id])?>"><?= $items->name; ?></a></li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="mainContainer">
                <div class="header">
                    <div class="logo"><a href="/"><img src="/images/logo.svg"/></a></div>
                    <nav class="headerMenu">
                        <ul>
                            <li class="<?= Yii::$app->controller->action->id == 'index' ? 'activeItem' : null ?>"><a href="/">Главная</a></li>
                            <li class="<?= Yii::$app->controller->action->id == 'video' ? 'activeItem' : null ?>"><a href="<?= Url::to(['/site/video'])?>">Видео</a></li>
                            <li class="
                            <?= Yii::$app->controller->action->id == 'service' ? 'activeItem' : null ?>
                            <?= Yii::$app->controller->action->id == 'service-view' ? 'activeItem' : null ?>
                                "><a href="<?= Url::to(['/site/service'])?>">Услуги</a></li>
                            <li class="<?= Yii::$app->controller->action->id == 'contacts' ? 'activeItem' : null ?>"><a href="<?= Url::to(['/site/contacts'])?>">Контакты</a></li>
                            <li class="<?= Yii::$app->controller->action->id == 'testdrive' ? 'activeItem' : null ?>"><a href="<?= Url::to(['/site/testdrive'])?>">Тестдрайв</a></li>
                        </ul>
                    </nav>
                    <button class="btnNav"><span></span></button>
                    <div class="headerButtons">
                        <a href="#" class="searchBtn">
                            <img src="/images/icons/search-ico.svg"/></a>
                        <a href="<?= Url::to(['/product/cart'])?>">
                            <img src="/images/icons/cart-ico.svg"/>
                            <span class="count">
                                <?= isset($_SESSION['cart']) ? count($_SESSION['cart']) : '0'?>
                            </span>
                        </a>
                    </div>
                    <div class="sn">
                        <?php if(!empty($social)):?>
                            <?php foreach ($social as $item): ?>
                        <a href="<?= $item->url ?>"><?= Html::img(Yii::getAlias('@web').'/uploads/'.$item->icon)?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- end #header-->
        <!-- start #content-->
        <div id="content">

            <?= $content ?>

        </div>
        <!-- end #content-->
    </div>
    <!-- end #mainWrap-->
    <!-- start #_footer-->
    <div id="footer">
        <div class="mainFooter">
            <div class="mainContainer">
                <div class="mainFooterTable">
                    <div class="mainFooterRow">
                        <div class="mainFooterCol mainFooterColFirst">
                            <div class="fLogo"><img src="/images/logo.svg"/></div>
                            <div class="fText">
                                <?php if(!empty($contacts)): ?>
                                    <?= $contacts->info; ?>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="mainFooterCol">
                            <div class="fColTitle">Навигация</div>
                            <nav class="fNav">
                                <ul>

                                    <li><a href="<?= Url::to(['/site/service'])?>">Услуги</a></li>
                                    <li><a href="<?= Url::to(['/site/gallery'])?>">Галерея</a></li>
                                    <li><a href="<?= Url::to(['/site/video'])?>">Видео</a></li>
                                    <li><a href="<?= Url::to(['/site/testdrive'])?>">Тестдрайв</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="mainFooterCol">
                            <div class="fColTitle">Контакты</div>
                            <div class="fContactsBlock">
                                <?php if(!empty($contacts)): ?>
                                <div class="telBlock">
                                    <a href="tel:+998<?=$contacts->phone;?>">+998 <span><?=$contacts->phone;?></span></a>
                                </div>
                                    <p><?=$contacts->working_days;?></p>
                                    <p><?=$contacts->working_times;?></p>
                                <?php endif; ?>
                                <div class="sn">
                                    <?php if(!empty($social)):?>
                                        <?php foreach ($social as $item): ?>
                                            <a href="<?= $item->url ?>"><?= Html::img(Yii::getAlias('@web').'/uploads/'.$item->icon)?></a>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="minFooter">
            <div class="mainContainer">
                <div class="footer">
                    <div class="textC">2018 © OOO «CITYCOCO». Все права защищены</div>
                    <div class="source">Создано <a href="#"><img src="/images/sourceLogo.svg"/><span>SOS</span></a></div>
                </div>
            </div>
        </div>
        <div id="maska"></div>
    </div>
    <!-- end #footer-->

</div>
    <div id="searchModal">
    <div class="searchModalMask"></div>
    <button class="searchModalClose"><img src="/images/icons/close.svg"/></button>
    <div class="searchModalForm">
        <div class="mainContainer">
            <form action="/product/search" method="get">
                <input id="searchField" type="text" name="q" placeholder="Поиск"/>
                <button type="submit" class="btnSearchSubmit"></button>
            </form>
        </div>
    </div>
</div>
<div class="loader hidden"><img src="/images/2.gif"/></div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
