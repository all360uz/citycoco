<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\Social;
use app\models\Contacts;

AppAsset::register($this);
    $social = Social::find()->all();
    $contacts = Contacts::find()->where(['id' => 1])->One();
$category = \app\models\Categories::find()->all();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="minBody">
    <!-- start #mainWrap-->
    <div id="mainWrap">
        <!-- start #header-->
        <div id="header">
            <div id="mobileNavBar">
                <div class="mobileNavBarWrapInner">
                    <div class="headerButtons"><a href="#" class="searchBtn"> <img src="/images/icons/search-ico.svg"/></a><a href="#"> <img src="/images/icons/cart-ico.svg"/><span class="count">3</span></a></div>
                    <div class="sn">
                        <?php if(!empty($social)):?>
                            <?php foreach ($social as $item): ?>
                                <a href="<?= $item->url ?>"><?= Html::img(Yii::getAlias('@web').'/uploads/'.$item->icon)?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <div class="clear"></div>
                    <nav class="headerMenu">
                        <ul>
                            <li><a href="<?= Url::to(['site/video'])?>">Главная</a></li>
                            <li><a href="<?= Url::to(['site/video'])?>">Видео</a></li>
                            <li><a href="<?= Url::to(['site/servise'])?>">Услуги</a></li>
                            <li><a href="<?= Url::to(['site/contacts'])?>">Контакты</a></li>
                            <li><a href="<?= Url::to(['site/testdrive'])?>">Тестдрайв</a></li>
                        </ul>
                    </nav>
                    <nav class="mainBlockBavBar">
                        <ul>
                            <?php if(!empty($category)):?>
                                <?php foreach ($category as $items): ?>
                                    <li><a href="<?=Url::to(['/product/category', 'id' => $items->id])?>"><?= $items->name; ?></a></li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="mainContainer">
                <div class="header">
                    <div class="logo"><a href="#"><img src="/images/logo.svg"/></a></div>
                    <nav class="headerMenu">
                        <ul>
                            <li><a href="<?= Url::to(['site/video'])?>">Главная</a></li>
                            <li><a href="<?= Url::to(['site/video'])?>">Видео</a></li>
                            <li><a href="<?= Url::to(['site/servise'])?>">Услуги</a></li>
                            <li><a href="<?= Url::to(['site/contacts'])?>">Контакты</a></li>
                            <li><a href="<?= Url::to(['site/testdrive'])?>">Тестдрайв</a></li>
                        </ul>
                    </nav>
                    <button class="btnNav"><span></span></button>
                    <div class="headerButtons">
                        <a href="#" class="searchBtn">
                            <img src="/images/icons/search-ico.svg"/></a>
                        <a href="<?= Url::to(['order/cart'])?>">
                            <img src="/images/icons/cart-ico.svg"/>
                            <span class="count">
                                <?= isset($_SESSION['cart']) ? count($_SESSION['cart']) : '0'?>
                            </span>
                        </a>
                    </div>
                    <div class="sn">
                        <?php if(!empty($social)):?>
                            <?php foreach ($social as $item): ?>
                        <a href="<?= $item->url ?>"><?= Html::img(Yii::getAlias('@web').'/uploads/'.$item->icon)?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- end #header-->
        <!-- start #content-->
        <div id="content">

            <?= $content ?>

        </div>
        <!-- end #content-->
    </div>
    <!-- end #mainWrap-->
    <!-- start #_footer-->


</div>
    <div id="searchModal">
    <div class="searchModalMask"></div>
    <button class="searchModalClose"><img src="/images/icons/close.svg"/></button>
    <div class="searchModalForm">
        <div class="mainContainer">
            <form action="">
                <input id="searchField" type="text" placeholder="Поиск"/>
                <button type="submit" class="btnSearchSubmit"></button>
            </form>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
