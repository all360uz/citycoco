<?php

namespace app\models;

use app\components\FileUploadBehaviour;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property int $related_product_id
 * @property string $img
 * @property int $is_main_page
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property BrandsCategories[] $brandsCategories
 * @property Brands[] $brands
 * @property Products[] $products
 */
class Categories extends \yii\db\ActiveRecord
{
    public $brands;
    public $schedule;
    public $file;
    
    /**
     * @inheritdoc
     */
    public static function tableName ()
    {
        return 'categories';
    }
    
    public function behaviors ()
    {
        return [

            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'img',
                'baseUrlAttribute' => false
            ]
        ];
    }
    
    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave ( $insert, $changedAttributes )
    {
        if ($this->brands) {
            $brands = array_unique($this->brands);
            if (!$this->isNewRecord)
                BrandsCategories::deleteAll(['categories_id' => $this->id]);
            foreach ($brands as $brand) {
                try {
                    Yii::$app->db->createCommand()->insert('brands_categories', [
                        'brands_id' => $brand,
                        'categories_id' => $this->id,
                    ])->execute();
                } catch ( Exception $e ) {
                }
            }
        }
    }
    
    /**
     * Load brands after find
     */
    public function afterFind ()
    {
        $links = BrandsCategories::findAll([ 'categories_id' => $this->id ]);
        foreach ( $links as $link ) {
            $this->schedule[] = $link->brands_id;
        }
    }
    
    /**
     * @inheritdoc
     */
    public function rules ()
    {
        return [
            [ [ 'name' ], 'required' ],
            [ [ 'status', 'related_product_id', 'is_main_page' ], 'integer' ],
            [ [ 'name', 'meta_keywords', 'meta_description', 'img' ], 'string', 'max' => 255 ],
//            [ [ 'img' ], 'file' ],
            [ ['brands', 'file'], 'safe' ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'status' => 'Статус',
            'related_product_id' => 'Связанный продукт при нажатии',
            'img' => 'Фото',
            'file' => 'Фото',
            'is_main_page' => 'Показать на главной странице?',
            'meta_keywords' => 'Мета-ключевые слова',
            'meta_description' => 'Мета-описание',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrandsCategories ()
    {
        return $this->hasMany(BrandsCategories::className(), [ 'categories_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductBrands ()
    {
        return $this->hasMany(Brands::className(), [ 'id' => 'brands_id' ])->viaTable('brands_categories', [ 'categories_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts ()
    {
        return $this->hasMany(Products::className(), [ 'category_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedProduct ()
    {
        return $this->hasOne(Products::className(), [ 'id' => 'related_product_id' ]);
    }
}
