<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property int $qty
 * @property int $delivery_types_id
 * @property int $created_at
 * @property int $status
 *
 * @property DeliveryTypes $deliveryTypes
 * @property Products $product
 * @property User $user
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id', 'qty', 'delivery_types_id', 'created_at', 'status'], 'integer'],
            [['delivery_types_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryTypes::className(), 'targetAttribute' => ['delivery_types_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'qty' => 'Qty',
            'delivery_types_id' => 'Delivery Types ID',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryTypes()
    {
        return $this->hasOne(DeliveryTypes::className(), ['id' => 'delivery_types_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

//     Add TO CART  - добавлении товара в сессию

    public function addToCart($product, $qty = 1) {

        if(!empty($product->discount)):
            $price = $product->discount;
        else: $price = $product->price;
        endif;

        if(isset($_SESSION['cart'][$product->id])){
            $_SESSION['cart'][$product->id]['qty'] += $qty;  // если товар уже есть, добавляет +1 к qty
        }
        else {
            $_SESSION['cart'][$product->id] = [
                'id' => $product->id,
                'qty' => $qty,
                'name' => $product->name,
                'price' => $product->price,
                'img' => $product->img,
                'discount' => $product->discount,
            ];
        }
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty'] + $qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum'] + $qty* $price : $qty *  $price;

    }
    public function minusToCart($product, $qty = 1) {
        if(!empty($product->discount)):
            $price = $product->discount;
        else: $price = $product->price;
        endif;

        if($_SESSION['cart'][$product->id]['qty'] != 0){
            $_SESSION['cart'][$product->id]['qty'] -= $qty;  // если товар уже есть, добавляет -1 к qty
        }

        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty'] - $qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum'] - $qty*$price : $qty * $price;

    }
// Calc product
    public function recalc($id) {

        if(!empty($_SESSION['cart'][$id]['discount'])):
            $price = $_SESSION['cart'][$id]['discount'];
        else: $price = $_SESSION['cart'][$id]['price'];
        endif;
        if(!isset($_SESSION['cart'][$id])) return false;
        $qtyMinus = $_SESSION['cart'][$id]['qty'];
        $sumMinus = $_SESSION['cart'][$id]['qty'] * $price;
        $_SESSION['cart.qty'] -= $qtyMinus;
        $_SESSION['cart.sum'] -= $sumMinus;
        unset($_SESSION['cart'][$id]);

    }




}
