<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_delivery_address".
 *
 * @property int $id
 * @property int $place_id
 * @property int $user_id
 * @property string $address
 * @property double $lat
 * @property double $lng
 * @property string $phone
 *
 * @property Orders[] $orders
 */
class UserDeliveryAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_delivery_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['lat', 'lng'], 'number'],
            [['address', 'place'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'place' => 'Place ID',
            'address' => 'Address',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'phone' => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['address_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
}
