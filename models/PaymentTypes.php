<?php

namespace app\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "payment_types".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $logo
 * @property int $status
 *
 * @property Orders[] $orders
 */
class PaymentTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;

    public static function tableName()
    {
        return 'payment_types';
    }


    /**
     * @inheritdoc
     */
    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'logo',
                'baseUrlAttribute' => false
            ],

        ];
    }
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name', 'description', 'logo'], 'string', 'max' => 255],
            [['file'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'logo' => 'Фото',
            'file' => 'Фото',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['payment_type' => 'id']);
    }
}
