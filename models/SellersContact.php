<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use trntv\filekit\behaviors\UploadBehavior;
use trntv\filekit\widget\Upload;

/**
 * This is the model class for table "sellers_contact".
 *
 * @property int $id
 * @property string $name
 * @property string $sourname
 * @property string $phone
 * @property string $img
 * @property string $email
 * @property string $position
 */
class SellersContact extends \yii\db\ActiveRecord
{

    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sellers_contact';
    }


    public function behaviors ()
    {
        return [
                [
                    'class' => UploadBehavior::className(),
                    'attribute' => 'file',
                    'pathAttribute' => 'img',
                    'baseUrlAttribute' => false
                ],

            ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sourname'], 'required'],
            [['name', 'sourname', 'phone', 'img', 'email', 'position'], 'string', 'max' => 255],
            [['file'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'sourname' => 'Фамилия',
            'phone' => 'Телефон номер',
            'img' => 'Фото',
            'file' => 'Фото',
            'email' => 'E-mail',
            'position' => 'Должность',
        ];
    }
}
