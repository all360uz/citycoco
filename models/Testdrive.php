<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "testdrive".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string $meta_keywords
 * @property string $meta_description
 */
class Testdrive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'testdrive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['meta_keywords', 'meta_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'content' => 'Текст',
            'meta_keywords' => 'Мета-ключевые слова',
            'meta_description' => 'Мета описание',
        ];
    }
}
