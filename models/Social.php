<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use trntv\filekit\behaviors\UploadBehavior;
use trntv\filekit\widget\Upload;

/**
 * This is the model class for table "social".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $icon
 * @property int $status
 */
class Social extends \yii\db\ActiveRecord
{

    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'social';
    }

    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'icon',
                'baseUrlAttribute' => false
            ],

        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['status'], 'integer'],
            [['name', 'url', 'icon'], 'string', 'max' => 255],
            [['file'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url' => 'Ссылка',
            'icon' => 'Иконка',
            'status' => 'Статус',
        ];
    }
}
