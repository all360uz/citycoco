<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use trntv\filekit\widget\Upload;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\Json;
use yii\db\Expression;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $name
 * @property string $img
 * @property int $price
 * @property double $discount
 * @property string $content
 * @property int $category_id
 * @property double $rating
 * @property int $brand_id
 * @property string $short_desc
 * @property string $main_params_json
 * @property int $created_at
 * @property int $updated_at
 * @property int $status
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property Cart[] $carts
 * @property OrdersProducts[] $ordersProducts
 * @property Orders[] $orders
 * @property ProductImages[] $productImages
 * @property Brands $brand
 * @property Categories $category
 * @property ProductsAddFilter[] $productsAddFilters
 * @property AddFilter[] $addFilters
 * @property ProductsMainFilter[] $productsMainFilters
 * @property MainFilter[] $mainFilters
 * @property ProductsProductParams[] $productsProductParams
 * @property ProductParams[] $productParams
 * @property ProductsRelations[] $productsRelations
 * @property ProductsRelations[] $productsRelations0
 * @property Products[] $relatedProducts
 * @property Products[] $products
 */
class Products extends ActiveRecord
{
    public $schedule;
    public $self_params;
    public $attachments;
    public $file;
    public $related_products;
    public $product_main_filter;
    public $product_add_filter;
    public $product_params;
    public $product_params_value;
    public $product_main_param;
    public $product_self_param;
    public $product_self_param_value;
    
    /**
     * @inheritdoc
     */
    public static function tableName ()
    {
        return 'products';
    }
    
    /**
     * @return array
     */
    public function behaviors ()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'immutable' => true
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'productImages',
                'pathAttribute' => 'img',
                'baseUrlAttribute' => false,
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'img',
                'baseUrlAttribute' => 'base_url',
                //'baseUrlAttribute' => false,
            ],

        ];
    }
    
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave ( $insert )
    {
        $json_data = [];
        if ( is_array($this->schedule) ) {
            foreach ( $this->schedule as $schedule ) {
                if ( $schedule['product_main_param'] ) {
                    $param = ProductParams::findOne($schedule['product_params']);
                    $json_data[] = [
                        $param->name => $schedule['product_params_value'],
                    ];
                }
            }
        }
        $this->main_params_json = Json::encode($json_data);
        
        return true;
    }
    
    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave ( $insert, $changedAttributes )
    {
        if ( is_array($this->schedule) ) {
            if (!$this->isNewRecord)
                ProductsProductParams::deleteAll(['products_id' => $this->id]);
            foreach ( $this->schedule as $schedule ) {
                if ( $schedule['product_params'] && $schedule['product_params_value'] ) {
                    try {
                        Yii::$app->db->createCommand()->insert('products_product_params', [
                            'products_id' => $this->id,
                            'product_params_id' => $schedule['product_params'],
                            'value' => $schedule['product_params_value'],
                            'is_main' => $schedule['product_main_param'],
                        ])->execute();
                    } catch ( Exception $e ) {
                        echo $e->getMessage();
                    }
                }
            }
        }
        
        if ( is_array($this->self_params) ) {
            if (!$this->isNewRecord)
                ProductSelfParams::deleteAll(['product_id' => $this->id]);
            foreach ( $this->self_params as $param ) {
                try {
                    Yii::$app->db->createCommand()->insert('product_self_params', [
                        'product_id' => $this->id,
                        'name' => $param['product_self_param'],
                        'value' => $param['product_self_param_value'],
                    ])->execute();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }
            }
        }
        
        if ( is_array($this->related_products) ) {
            if (!$this->isNewRecord)
                ProductsRelations::deleteAll(['product_id' => $this->id]);
            foreach ( $this->related_products as $relation ) {
                try {
                    Yii::$app->db->createCommand()->insert('products_relations', [
                        'product_id' => $this->id,
                        'related_product_id' => $relation,
                    ])->execute();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }
            }
        }
        
        if ( is_array($this->product_main_filter) ) {
            if (!$this->isNewRecord)
                ProductsMainFilter::deleteAll(['products_id' => $this->id]);
            foreach ( $this->product_main_filter as $main_filter ) {
                try {
                    Yii::$app->db->createCommand()->insert('products_main_filter', [
                        'products_id' => $this->id,
                        'main_filter_id' => $main_filter,
                    ])->execute();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }
            }
        }
        
        if ( is_array($this->product_add_filter) ) {
            if (!$this->isNewRecord)
                ProductsAddFilter::deleteAll(['products_id' => $this->id]);
            foreach ( $this->product_add_filter as $add_filter ) {
                try {
                    Yii::$app->db->createCommand()->insert('products_add_filter', [
                        'products_id' => $this->id,
                        'add_filter_id' => $add_filter,
                    ])->execute();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }
            }
        }
        
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind ()
    {
        $main_filter = ProductsMainFilter::find()->where(['products_id' => $this->id])->all();
        foreach ($main_filter as $item):
            $this->product_main_filter[] = $item->main_filter_id;
        endforeach;


        $add_filter = ProductsAddFilter::find()->where(['products_id' => $this->id])->all();
        foreach ($add_filter as $item):
            $this->product_add_filter[] = $item->add_filter_id;
        endforeach;

        $product_params = ProductsProductParams::find()->where(['products_id' => $this->id])->all();
        $k = 0;
        foreach ($product_params as $item):
            $this->schedule[$k]['product_params'][] = $item->product_params_id;
            $this->schedule[$k]['product_params_value'] = $item->value;
            $this->schedule[$k]['product_main_param'] = $item->is_main;
            $k ++;
        endforeach;

        $self_params = ProductSelfParams::find()->where(['product_id' => $this->id])->all();
        $j = 0;
        foreach ($self_params as $item):
            $this->self_params[$j]['product_self_param'] = $item->name;
            $this->self_params[$j]['product_self_param_value'] = $item->value;
            $j ++;
        endforeach;

        $related_products = ProductsRelations::find()->where(['product_id' => $this->id])->all();
        foreach ($related_products as $item):
            $this->related_products[] = $item->related_product_id;
        endforeach;
    }
    
    /**
     * @inheritdoc
     */
    public function rules ()
    {
        return [
            [ [ 'name', 'price', 'category_id', 'brand_id'], 'required' ],
            [ [ 'price', 'category_id', 'brand_id', 'created_at', 'updated_at', 'status' ], 'integer' ],
            [ [ 'discount', 'rating' ], 'number' ],
            [ [ 'content', 'short_desc', 'main_params_json' ], 'string' ],
            [ [ 'name', 'meta_keywords', 'meta_description', 'img', 'slug' ], 'string', 'max' => 255 ],
            [ [ 'brand_id' ], 'exist', 'skipOnError' => true, 'targetClass' => Brands::className(), 'targetAttribute' => [ 'brand_id' => 'id' ] ],
            [ [ 'category_id' ], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => [ 'category_id' => 'id' ] ],
            [ [ 'attachments', 'file', 'related_products', 'product_main_filter', 'product_add_filter', 'schedule', 'self_params' ], 'safe' ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'img' => 'Фото',
            'price' => 'Цена',
            'discount' => 'Скидечная цена',
            'content' => 'Контент',
            'category_id' => 'Котегория',
            'rating' => 'Рейтинг',
            'brand_id' => 'Бренд',
            'short_desc' => 'Краткое описание',
            'main_params_json' => 'Оснавные параметры',
            'self_params' => 'Индивидуальные параметры',
            'created_at' => 'Создан',
            'updated_at' => 'Обнавлен',
            'status' => 'Статус',
            'meta_keywords' => 'Мета-ключевые слова',
            'meta_description' => 'Мета описание',
            'product_main_filter' => 'Выберите филтеры',
            'file' => 'Главное фото продукта',
            'attachments' => 'Отсальные фотографии продукта',
            'related_products' => 'Cопутствующие товары',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarts ()
    {
        return $this->hasMany(Cart::className(), [ 'product_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersProducts ()
    {
        return $this->hasMany(OrdersProducts::className(), [ 'products_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders ()
    {
        return $this->hasMany(Orders::className(), [ 'id' => 'orders_id' ])->viaTable('orders_products', [ 'products_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages ()
    {
        return $this->hasMany(ProductImages::className(), [ 'product_id' => 'id' ]);
    }

    public function getReviews ()
    {
        return $this->hasMany(ProductReview::className(), [ 'product_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand ()
    {
        return $this->hasOne(Brands::className(), [ 'id' => 'brand_id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory ()
    {
        return $this->hasOne(Categories::className(), [ 'id' => 'category_id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsAddFilters ()
    {
        return $this->hasMany(ProductsAddFilter::className(), [ 'products_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddFilters ()
    {
        return $this->hasMany(AddFilter::className(), [ 'id' => 'add_filter_id' ])->viaTable('products_add_filter', [ 'products_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsMainFilters ()
    {
        return $this->hasMany(ProductsMainFilter::className(), [ 'products_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainFilters ()
    {
        return $this->hasMany(MainFilter::className(), [ 'id' => 'main_filter_id' ])->viaTable('products_main_filter', [ 'products_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsProductParams ()
    {
        return $this->hasMany(ProductsProductParams::className(), [ 'products_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductParams ()
    {
        return $this->hasMany(ProductParams::className(), [ 'id' => 'product_params_id' ])->viaTable('products_product_params', [ 'products_id' => 'id' ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getProductSelfParams()
    {
        return $this->hasMany(ProductSelfParams::className(), ['product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsRelations ()
    {
        return $this->hasMany(ProductsRelations::className(), [ 'product_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsRelations0 ()
    {
        return $this->hasMany(ProductsRelations::className(), [ 'related_product_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedProducts ()
    {
        return $this->hasMany(Products::className(), [ 'id' => 'related_product_id' ])->viaTable('products_relations', [ 'product_id' => 'id' ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts ()
    {
        return $this->hasMany(Products::className(), [ 'id' => 'product_id' ])->viaTable('products_relations', [ 'related_product_id' => 'id' ]);
    }

}
