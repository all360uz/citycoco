<?php

namespace app\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "photo_gallery".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $img
 * @property int $created_at
 * @property int $updated_at
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property GalleryImages[] $galleryImages
 */
class PhotoGallery extends \yii\db\ActiveRecord
{
    public $attachments;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo_gallery';
    }

    public function behaviors ()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => '\app\components\FileUploadBehaviour'
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'galleryImages',
                'pathAttribute' => 'img',
                'baseUrlAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'description', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
            [ [ 'img' ], 'file' ],
            [ [ 'attachments' ], 'safe' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'img' => 'Фото',
            'created_at' => 'Создан',
            'updated_at' => 'Обнавлено',
            'meta_keywords' => 'Мета-ключевые слова',
            'meta_description' => 'Мета-описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryImages()
    {
        return $this->hasMany(GalleryImages::className(), ['gallery_id' => 'id']);
    }
}
