<?php

namespace app\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\widget\Upload;
use yii\db\Exception;
use yii\helpers\Json;


/**
 * This is the model class for table "gallery_images".
 *
 * @property int $id
 * @property string $img
 * @property int $gallery_id
 *
 * @property PhotoGallery $gallery
 */
class GalleryImages extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery_images';
    }

    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'img',
                'baseUrlAttribute' => false
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [ [ 'gallery_id'], 'required'],
            [ [ 'gallery_id'], 'integer'],
            [ [ 'img' ], 'string', 'max' => 255 ],
            [ [ 'file' ], 'safe' ],
            [ [ 'gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => PhotoGallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => 'Фото',
            'file' => 'Фото',
            'gallery_id' => 'Галерея',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(PhotoGallery::className(), ['id' => 'gallery_id']);
    }
}
