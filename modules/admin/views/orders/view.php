<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'amount',
                'value' => function($model)
                {
                    return number_format($model->amount, '0',' ',' ');
                }
            ],
            [
                'attribute' => 'address_id',
                'value' => function($model)
                {
                    return $model->address->address;
                }
            ],
            [
                'attribute' => 'start_date',
                'value' => function($model){
                    return date('M d, Y',$model->start_date);
                }
            ],
            [
                'attribute' => 'updated_date',
                'value' => function($model){
                    return date('M d, Y',$model->start_date);
                }
            ],
//            'updated_date',
            'phone',
            [
                    'attribute' => 'payment_type',
                    'value' => function($model){
                        return $model->paymentType->name;
                    }
            ],
            [
                'attribute' => 'delivery_type',
                'value' => function($model){
                    return $model->deliverytType->name;
                }
            ],[
                'attribute' => 'state',
                'value' => function($model){
                    if($model->state == 0):
                        return 'В ожидании';
                    else:
                        return 'Сделан';
                    endif;

                }
            ],
        ],
    ]) ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Адрес доставки: <?= $model->address->place; ?>, <?= $model->address->address?></h4>
                </div>
                <div class="card-body">
                    <input id="lat" class="hidden" value="<?= $model->address->lat; ?>" />
                    <input id="lng" class="hidden" value="<?= $model->address->lng; ?>" />
                    <div class="mapsColl">

                        <div class="field" id="map3" style="height: 300px; width: 100% ">
                            <script>



                                function initMap( ) {
                                    var lat1 = Number(document.getElementById("lat").value);
                                    var lng1 = Number(document.getElementById("lng").value);
                                    var uluru = {lat: lat1, lng: lng1};
                                    var map = new google.maps.Map(document.getElementById('map3'), {
                                        zoom: 15,
                                        center: uluru

                                    });
                                    var marker = new google.maps.Marker({

                                        position: uluru,
                                        map: map
                                    });
                                }


                            </script>
                            <script async defer
                                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
                            </script>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Продукты заказа № <?= $model->id; ?></h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID продукта</th>
                                <th>Название</th>
                                <th>Цена</th>
                                <th>Кол-во</th>
                                <th>Сумма</th>
                                <th>Категория</th>
                                <th>Посмотреть</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($model->ordersProducts as $products): ?>
                                <tr>
                                    <td><?= $products->id; ?></td>
                                    <td><?= $products->products->name; ?></td>
                                    <td><?= number_format($products->product_price, '0',' ',' '); ?> сум</td>
                                    <td><?= $products->quantity; ?></td>
                                    <td><?= number_format($products->discount, '0',' ',' '); ?> сум</td>
                                    <td><?= $products->products->category->name; ?></td>
                                    <td>
                                        <a href="<?= \yii\helpers\Url::to(['/product/product', 'slug' => $products->products->slug]); ?>">
                                        <i class="fa fa-eye"></i></a>
                                    </td>

                                </tr>
                            <?php endforeach;?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
