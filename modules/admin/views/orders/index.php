<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'phone',
            [
                    'attribute' => 'amount',
                    'value' => function($model)
                    {
                        return number_format($model->amount, '0',' ',' ');
                    }
            ],
            [
                    'attribute' => 'address_id',
                    'value' => function($model)
                    {
                        return $model->address->address;
                    }
            ],
            [
                    'attribute' => 'start_date',
                    'value' => function($model){
                        return date('M d, Y',$model->start_date);
                    }
            ],
            [
                    'attribute' => 'state',
                    'value' => function($model){
                         if($model->state == 0):
                             return '<span style="color: #ff0c3e;">В ожидании</span>';
                         else:
                             return '<span style="color: #0b58a2">Сделан</span>';

                          endif;

                    },
                    'format' => 'html',
            ],
            //'updated_date',

            //'payment_type',
            //'delivery_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
