<?php
/**
 * Created by PhpStorm.
 * User: Farhodjon
 * Date: 10.03.2018
 * Time: 15:17
 */

use app\modules\admin\widgets\Menu;

?>
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <?php
            try {
                echo Menu::widget([
                    'options' => [ 'id' => 'sidebarnav' ],
                    'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                    'badgeClass' => 'label label-rouded label-primary pull-right',
                    'activateParents' => true,
                    'items' => [
                        [
                            'label' => '',
                            'options' => [ 'class' => 'nav-devider' ]
                        ],
                        [
                            'label' => 'Home',
                            'options' => [ 'class' => 'nav-label' ]
                        ],
                        [
                            'label' => 'Главная',
                            'url' => ['default/index'],
                            'icon' => '<i class="fa fa-home"></i>',
                        ],
                        [
                            'label' => 'Apps',
                            'options' => ['class' => 'nav-label']
                        ],
                        [
                            'label' => 'Контент',
                            'url' => '#',
                            'icon' => '<i class="fa fa-file-text-o"></i>',
                            'items' => [
                                [
                                    'label' => 'Слайдер',
                                    'url' => ['widget-carousel/index']
                                ],
                                [
                                    'label' => 'Галарея',
                                    'url' => ['photo-gallery/index']
                                ],
                                [
                                    'label' => 'Фотографии',
                                    'url' => ['gallery-images/index']
                                ],

                            ]
                        ],
                        [
                            'label' => 'Продукты',
                            'url' => '#',
                            'icon' => '<i class="fa fa-archive"></i>',
                            'items' => [
                                [
                                    'label' => 'Категории продуктов',
                                    'url' => ['categories/index']
                                ],
                                [
                                    'label' => 'Бренды',
                                    'url' => ['brands/index']
                                ],
                                [
                                    'label' => 'Продукты',
                                    'url' => ['products/index']
                                ],
                                [
                                    'label' => 'Главный фильтр',
                                    'url' => ['main-filter/index']
                                ],
                                [
                                    'label' => 'Дополнительный фильтр',
                                    'url' => ['add-filter/index']
                                ],
                                [
                                    'label' => 'Сортировка продуктов',
                                    'url' => ['product-sort-attrs/index']
                                ],
                                [
                                    'label' => 'Параметры продуктов',
                                    'url' => ['product-params/index']
                                ],
                                [
                                    'label' => 'Отзывы',
                                    'url' => ['product-review/index']
                                ],
                                [
                                    'label' => 'Тип доставки',
                                    'url' => ['delivery-types/index']
                                ],
                                [
                                    'label' => 'Тип оплаты',
                                    'url' => ['payment-types/index']
                                ],
                            ]
                        ],
                        [
                            'label' => 'Страницы',
                            'url' => '#',
                            'icon' => '<i class="fa fa-file-text-o"></i>',
                            'items' => [

                                [
                                    'label' => 'Видео',
                                    'url' => ['video-gallery/index']
                                ],
                                [
                                    'label' => 'Услуги',
                                    'url' => ['articles/index']
                                ],
                                [
                                    'label' => 'Контакты',
                                    'url' => ['sellers-contact/index']
                                ],
                                [
                                    'label' => 'Тестдрайв',
                                    'url' => ['testdrive/view', 'id' => 1]
                                ],



                            ]
                        ],
                        [
                            'label' => 'Футер',
                            'url' => '#',
                            'icon' => '<i class="fa fa-file-text-o"></i>',
                            'items' => [
                                [
                                    'label' => 'Контакты CityCoco',
                                    'url' => ['contacts/view', 'id' => 1]
                                ],
                                [
                                    'label' => 'Социальные сети',
                                    'url' => ['social/index']
                                ],


                            ]
                        ],
                        [
                            'label' => 'Заказы',
                            'icon' => '<i class="fa fa-reorder"></i>',
                            'url' => ['orders/index']
                        ],
                    ]
                ]);
            } catch ( Exception $e ) {
            }
            
            ?>
        </nav>
    </div>
</div>
