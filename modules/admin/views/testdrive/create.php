<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Testdrive */

$this->title = 'Create Testdrive';
$this->params['breadcrumbs'][] = ['label' => 'Testdrives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testdrive-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
