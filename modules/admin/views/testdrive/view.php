<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Testdrive */

$this->title = $model->title;

?>
<div class="testdrive-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
       <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'title',
            'content:ntext',
            'meta_keywords',
            'meta_description',
        ],
    ]) ?>

</div>
