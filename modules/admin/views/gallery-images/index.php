<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\PhotoGallery;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\GalleryImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фотографии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-images-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'img',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::img('/uploads/' . $model->img, ['style' => 'max-width: 150px']);
                }
            ],
            [
                    'attribute' => 'gallery_id',
                    'value' => function ($model){
                        return $model->gallery->name;
                    }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
