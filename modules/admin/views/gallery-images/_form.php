<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\PhotoGallery;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model app\models\GalleryImages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-images-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'file')->widget(
        Upload::className(),
        [
            'url' => ['/admin/file-storage/upload'],
            'maxFileSize' => 5000000, // 5 MiB
        ]);
    ?>

    <?= $form->field($model, 'gallery_id')->dropDownList(ArrayHelper::map(PhotoGallery::find()->all(), 'id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
