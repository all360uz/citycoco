<!-- Start Page Content -->
<div class="row">
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-usd f-s-40 color-primary"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= number_format($sum, '0', ' ',' ') ?> сум</h2>
                    <p class="m-b-0">Общая сумма заказов</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= number_format($qty, '0', ' ',' ') ?></h2>
                    <p class="m-b-0">Проданные продукты</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-hourglass-end f-s-40 color-warning"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= number_format($pending, '0', ' ',' ') ?></h2>
                    <p class="m-b-0">Заказы в ожидании</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa  fa-database f-s-40 color-danger"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= number_format($products, '0', ' ',' ') ?></h2>
                    <p class="m-b-0">Продукты на складе</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title">
                <h4>Недавние заказы</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>№ заказа</th>
                            <th>Телефон номер</th>
                            <th>Общая сумма</th>
                            <th>Дата</th>
                            <th>Адрес</th>
                            <th>Тип доставки</th>
                            <th>Статус</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($orders as $order): ?>
                            <tr>
                                <td><?= $order->id; ?></td>
                                <td>+<?= $order->phone; ?></td>
                                <td><span><?= number_format($order->amount, '0',' ',' '); ?> сум</span></td>
                                <td><span><?= date('M d, Y',$order->start_date); ?></span></td>
                                <td><span><?= $order->address->address; ?></span></td>
                                <td><span>
                                        <?= $order->deliverytType->price == 0 ? "Бесплатно" : number_format($order->deliverytType->price, '0', ' ',' ').' сум'; ?>
                                    </span></td>

                                <td><?php if($order->state == 0): ?>
                                      <span class="badge badge-warning">В ожидании</span>
                                    <?php else: ?>
                                      <span class="badge badge-success">Сделан</span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-8">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Последные отзывы к продуктам</h4>
                    </div>
                    <div class="recent-comment">
                        <?php foreach ($reviews as $item): ?>
                        <div class="media">

                            <div class="media-body">
                                <h4 class="media-heading"><?= $item->name; ?></h4>
                                <p>Оценка: <?= $item->rate;?> </p>
                                <p>Отзыв: <?= $item->review; ?> </p>
                                <p class="comment-date"><?= date('M d, Y', $item->created_at); ?></p>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- /# card -->
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card bg-dark">
            <div class="testimonial-widget-one p-17">
                <div class="testimonial-widget-one owl-carousel owl-theme">
                    <?php foreach ($product as $item): ?>
                    <div class="item">
                        <div class="testimonial-content">
                            <img class="testimonial-author-img" src="/uploads/<?= $item->img ?>" alt="" />
                            <div class="testimonial-author"><?= $item->name ?></div>
                            <div class="testimonial-author-position"><?= $item->category->name ?></div>

                            <div class="testimonial-text">
                                <i class="fa fa-quote-left"></i>  <?= $item->short_desc; ?>
                                <i class="fa fa-quote-right"></i>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- End PAge Content -->