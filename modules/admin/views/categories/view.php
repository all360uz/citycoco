<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                //            'id',
                'name',
                [
                    'attribute' => 'status',
                    'value' => function($model)
                    {
                        return $model->status == 1 ? 'Опубликовоно' : 'Не опубликовоно';
                    }
                ],
                [
                    'attribute' => 'relatedProduct.name',
                    'label' => 'Связанный продукт при нажатии'
                ],

                [
                    'attribute' => 'img',
                    'format' => 'html',
                    'value' => function ($model) {
                        if (!empty($model->img)):
                            return Html::img('/uploads/' . $model->img, ['style' => 'max-width: 200px']);
                        else:
                            return 'Без фото';
                        endif;
                        }
                ],
                [
                    'attribute' => 'is_main_page',
                    'value' => function($model)
                    {
                        return $model->status == 1 ? 'Показать' : 'Не показать';
                    }
                ],
                'meta_keywords',
                'meta_description',
            ],
        ]);
    } catch ( Exception $e ) {
        echo $e->getMessage();
    } ?>

</div>
