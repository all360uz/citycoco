<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTypes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Тип оплаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-types-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить '.$model->name.'?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description',
            [
                'attribute' => 'logo',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::img('/uploads/' . $model->logo, ['style' => 'max-width: 100px']);
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($model)
                {
                    return $model->status == 1 ? 'Опубликовоно' : 'Не опубликовоно';
                }
            ],
        ],
    ]) ?>

</div>
