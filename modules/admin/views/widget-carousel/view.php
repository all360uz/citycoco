<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WidgetCarousel */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-carousel-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'id',
                [
                    'attribute' => 'img',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Html::img('/uploads/' . $model->img, ['style' => 'max-width: 200px']);
                    }
                ],
                'url:url',
                'small_caption',
                'main_caption',
                'content:html',
                [
                    'attribute' => 'status',
                    'value' => function($model)
                    {
                        return $model->status == 1 ? 'Опубликовоно' : 'Не опубликовоно';
                    }
                ],
                'order',
                'key',
                'created_at:datetime',
                'updated_at:datetime',
            ],
        ]);
    } catch ( Exception $e ) {
    } ?>

</div>
