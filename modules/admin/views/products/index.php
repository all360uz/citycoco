<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать продукт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            [
                'attribute' => 'img',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::img('/uploads/' . $model->img, ['style' => 'max-width: 100px']);
                }
            ],
            [
                'attribute' => 'price',
                'value' =>  function($data) {
                    $price = number_format($data->price, 0, ',', ' ') . ' сум';
                    return $price;
                },
                'format' => 'html',

            ],

//            [
//                'attribute' => 'discount',
//                'value' =>  function($data) {
//                        $price = number_format($data->discount, 0, ',', ' ') . ' сум';
//                        return $data->discount ? $price : 'Не задано';
//                },
//                'format' => 'html',
//
//            ],
            [
                    'attribute' => 'category_id',
                    'value' => function($model){
                        return $model->category->name;
                    }
            ],
            'slug',
            //'content:ntext',

            //'rating',
            //'brand_id',
            //'short_desc:ntext',
            //'main_params_json:ntext',
            //'created_at',
            //'updated_at',
            //'status',
            //'meta_keywords',
            //'meta_description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
