<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить продукт '.$model->name.'?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            [
                'attribute' => 'img',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::img('/uploads/' . $model->img, ['style' => 'max-width: 200px']);
                }
            ],
//            'price:currency',
            [
                'attribute' => 'price',
                'value' =>  function($data) {
                    $price = number_format($data->price, 0, ',', ' ') . ' сум';
                    return $price;
                },
                'format' => 'html',

            ],
            [
                'attribute' => 'discount',
                'value' =>  function($model) {
                    if(!empty($model->discount)):
                        $price = number_format($model->discount, 0, ',', ' ') . ' сум';
                        return $price;
                    else:
                        return 'Без скидки';
                    endif;
                },
                'format' => 'html',
            ],
//            'discount',
            [
                    'attribute' => 'content',
                    'format' => 'html',

            ],
            [
                    'attribute' => 'category_id',
                    'value' => $model->category->name,

            ],
//            'category_id',
            'rating',
            [
                'attribute' => 'brand_id',
                'value' => $model->brand->name,

            ],
            'short_desc:ntext',
            [
                    'attribute' => 'main_params_json',
                    'value' => function($model){
                        $return = '';
                        foreach ($model->productsProductParams as $item){
                            $return .= '<b>'.$item->productParams->name. '</b>: '.$item->value . '<br>';
                        }
                        return $return;
                    },
                    'format' => 'html',
            ],
            [
                'attribute' => 'self_params',
                'value' => function($model){
                    $return = '';
                    foreach ($model->productSelfParams as $item){
                        $return .= '<b>'.$item->name. '</b>: '.$item->value . '<br>';
                    }
                    return $return;
                },
                'format' => 'html',
            ],
//
            //'self_params',
            'created_at',
            'updated_at',
            [
                'attribute' => 'status',
                'value' => function($model)
                {
                    return $model->status == 1 ? 'Опубликовоно' : 'Не опубликовоно';
                }
            ],
            'meta_keywords',
            'meta_description',
        ],
    ]) ?>

</div>
