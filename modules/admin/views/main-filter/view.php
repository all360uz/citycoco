<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MainFilter */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Главный фильтр', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-filter-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы дейсвительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            [
                'attribute' => 'status',
                'value' => function($model)
                {
                    return $model->status == 1 ? 'Опубликовоно' : 'Не опубликовоно';
                }
            ],

        ],
    ]) ?>

</div>
