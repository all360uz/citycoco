<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тип доставки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-types-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'description',
            [
                    'attribute' => 'price',
                    'value' => function($data){
                        return $data->price == 0 ? 'Бесплатно' : $data->price;
                    },
            ],
            [
                'attribute' => 'status',
                'value' => function($data){
                    return $data->status == 0 ? 'Нет' : 'Да';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
