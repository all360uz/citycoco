<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DeliveryTypes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Тип доставки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-types-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            [
                'attribute' => 'price',
                'value' => function($data){
                    return $data->price == 0 ? 'Бесплатно' : $data->price;
                },
            ],
            [
                'attribute' => 'status',
                'value' => function($data){
                    return $data->status == 0 ? 'Нет' : 'Да';
                },
            ],
        ],
    ]) ?>

</div>
