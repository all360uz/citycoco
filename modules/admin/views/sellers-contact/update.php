<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SellersContact */

$this->title = 'Изменить: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sellers Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="sellers-contact-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
