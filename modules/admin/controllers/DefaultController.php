<?php

namespace app\modules\admin\controllers;

use app\models\ProductReview;
use app\models\Products;
use mdm\admin\models\form\Login;
use Yii;
use yii\web\Controller;
use app\models\Orders;
use app\models\OrdersProducts;
use app\models\UserDeliveryAddress;
use app\models\Sms;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $orders = Orders::find()->limit(10)->all();
        $sum = Orders::find()->sum('amount');
        $qty = OrdersProducts::find()->sum('quantity');
        $pending = Orders::find()->where(['state' => 0])->count();
        $products = Products::find()->count();
        $product = Products::find()->limit(4)->orderBy(['id'=> SORT_DESC])->all();
        $reviews = ProductReview::find()->limit(4)->orderBy(['id'=> SORT_DESC])->all();
        return $this->render('index', compact([
            'sum',
            'qty',
            'pending',
            'products',
            'orders',
            'reviews',
            'product',
        ]));
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $model = new Login();

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
}
