var MagicScrollOptions = {
    orientation: 'vertical',
    // items:' [ [480,2], [1900,2]',
    step: '1',
    // loop: 'rewind'
};


(function($) {
	$.fn.tabs = function( body , colback){
		var boddy = $(body);
		var navs = $(this).find('a');
		navs.click(function(e){
			e.preventDefault();
			var id = $(this).attr('data-href');
			$(this).parent().addClass('activeItem').parent().find('li').not($(this).parent()).removeClass('activeItem');
			boddy.each(function(){
				$(this).hide();
			});
			$(body+id).fadeIn();
      if (colback) {
        colback();
      }
		});
	}
})(jQuery);

(function($) {
	$.fn.tabsScroll = function(){
		var _ = $(this);
		function scrollItem(){
			if(_.length > 0){
				var active = _.find('li.active');
				var left = active.position().left - 50;
				_.animate({scrollLeft: left},200);		
			}
		}
		$(function(){
			scrollItem();
		});
		_.find('a').click(function(){
			scrollItem();
		});
	}
})(jQuery);

$.fn.hasAttr = function(name) {  
   return this.attr(name) !== undefined;
};

function showModal(modal) {
		modal.fadeIn();
		$('body').addClass('openModal');
	}
function hideModal(modal) {
	modal.fadeOut();
	$('body').removeClass('openModal');
}

$(document).ready(function(){

	$('a, button').click(function(e){
		if ($(this).hasAttr('data-modal')) {
			e.preventDefault();
			var modal = $(this).attr('data-modal');
			if ($(modal).length > 0) {
				showModal($(modal));
			}else{
				console.log('Объект не найден!')
			}
		}
	});

	$('.modalWindowMask, .modalWindowClose').click(function(){
		hideModal($(this).parents('.modalWindow'));
	});

	if ($('#mainSlider .mainSliderItem').length > 1) {
		$('#mainSlider').owlCarousel({
	    items: 1,
	    loop: true,
	    autoplay: true,
	    autoplayTimeout: 4000,
			smartSpeed: 800,
			dots: true,
			nav: true,
			navText: [ "<i class='sliderArrorBottom'></i>", "<i class='sliderArrorTop'></i>" ],
	  });
	}
	if ($('.dSlider').length > 0) {
		$('.dSlider').owlCarousel({
	    items: 3,
	    loop: true,
	    autoplay: true,
	    autoplayTimeout: 4000,
			smartSpeed: 800,
			dots: false,
			nav: true,
			navText: [ "<i class='sliderArrorBottom'></i>", "<i class='sliderArrorTop'></i>" ],
			responsive : {
				0:{
					items: 1
				},
				480: {
					items: 2
				},
				767: {
					items: 3
				}
			}
	  });
	}
	
	$('.searchBtn').click(function(e){
		e.preventDefault();
		showModal($('#searchModal'));
		$('#searchField').focus();
	});	

	$('.searchModalClose, .searchModalMask').click(function(e){
		e.preventDefault();
		hideModal($('#searchModal'));
	});

	function navItemContentSetHeight() {
		$('.navItemContent').each(function(){
			$(this).css({height:$('#mainBlockNav').height()});
		});
	}
	navItemContentSetHeight();
	$(window).on('load resize',function(){
		navItemContentSetHeight();
	});

	$('#mainBlockNav ul li a').click(function(e){
		e.preventDefault();
		var href = $(this).attr('href');
		if ($(this).parent().hasClass('activeItem')) {
			$('.navItemContent').hide();
			$(this).parents('ul').find('li').removeClass('activeItem');
			$('#maska').fadeOut();
		}else{
			$('.navItemContent').hide();
			$(this).parents('ul').find('li').removeClass('activeItem');
			$(this).parent().addClass('activeItem');
			$(href).show();
			$('#maska').fadeIn();
		}
	});
	$('#maska').click(function(e){
		e.preventDefault();
		$('.navItemContent').hide();
		$(this).fadeOut();
		$('#mainBlockNav ul li').removeClass('activeItem');
		$('body').removeClass('openMenu');
		$('body').removeClass('openFilter');
		$('.btnNav').removeClass('active');
	});

	$('.btnNav').click(function(e){
		e.preventDefault();
		if(!$(this).hasClass('active')){
			$(this).addClass('active');
			$('body').addClass('openMenu');
			$('#maska').fadeIn();
		}else{
			$(this).removeClass('active');
			$('body').removeClass('openMenu');
			$('#maska').fadeOut();
		}
	});

	$('.clearField').click(function(e){
		e.preventDefault();
		var field = $(this).attr('data-field');
		$(field).val('');
	});

	$('.clearField').each(function(e){
		var field = $(this).attr('data-field');
		var _ = $(this);
		if ($(field).val().length < 1) {
			$(this).hide();
		}
		$(field).on('input', function(){
			var vLen = $(this).val().length;
			if (vLen > 0) {
				_.show();
			}else{
				_.hide();
			}
		})
	});

	$('.num').each(function(){
		var _ = $(this);
		var plus = _.find('.nPlus');
		var minus = _.find('.nMinus');
		var val = _.find('.val');
		plus.click(function(e){
			e.preventDefault();
			var v = val.val();
			var nv = ++v;
			val.val(nv); 
			if(nv > 1){
				minus.removeClass('disabled_');
			}
		});
		minus.click(function(e){
			e.preventDefault();
			if(!$(this).hasClass('disabled_')){
				var v = val.val();
				var nv = --v;
				val.val(nv);
				if(nv <= 1){
					$(this).addClass('disabled_');
				} 	
			}
		});
	});

	$('.productTabNav').tabs('.pTabBody', function(){
		$('html, body').animate({scrollTop: $('.productTabNav').offset().top});
	});

	$('.new_stars').each(function(){
		var lli = $(this).find('li');
		var leng;
		$(this).hover(function(){
			leng = $('.star_on').length-1;
			console.log(leng);
			lli.removeClass('star_on');
		},function(){
			lli.removeClass('star_hover');
			if (leng >= 0) {
				$(this).find('li').eq(leng).addClass('star_on').prevAll('li').addClass('star_on');
			}
		});
		lli.hover(function(){
			$(this).addClass('star_hover').prevAll('li').addClass('star_hover');
			$(this).nextAll('li').removeClass('star_hover');
		});
		lli.click(function(){
			var n = $(this).attr('title');
			$(this).addClass('star_on').prevAll('li').addClass('star_on');
			$(this).nextAll('li').removeClass('star_on');
			$(this).parents('.new_stars').find('input').attr('value', n);
			leng = $('.star_on').length-1;
		});
	});

	$('.sortMore .sortMoreBtn').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('active')) {
			$(this).parents('.sortMore').find('ul').hide('fast');
			$(this).removeClass('active');
		}else{
			$(this).parents('.sortMore').find('ul').show('fast');
			$(this).addClass('active');
		}
		return false;
	});
	$(document).click(function(){
		$('.sortMore').find('ul').hide('fast');
		$('.sortMoreBtn').removeClass('active');
	});

	$('.styler').styler();

	$('.filterTitle').click(function(e){
		if ($(this).hasClass('closed')) {
			$(this).removeClass('closed');
			$(this).parents('.filterBox').find('.filterCont').slideDown();
		}else{
			$(this).addClass('closed');
			$(this).parents('.filterBox').find('.filterCont').slideUp();
		}
	});

	$('#filterBtn').click(function(e){
		e.preventDefault();
		$('body').addClass('openFilter');
		$('#maska').fadeIn();
	});

	if ($('.fancybox').length > 0) {
      $('.fancybox').fancybox({
      	loop: true,
      	thumbs : {
					autoStart   : true,                  // Display thumbnails on opening
					// hideOnClose : true,                   // Hide thumbnail grid when closing animation starts
					// parentEl    : '.fancybox-container',  // Container is injected into this element
					// axis        : 'y'                     // Vertical (y) or horizontal (x) scrolling
				},
				buttons : [
	        // 'slideShow',
	        // 'fullScreen',
	        // 'thumbs',
	        // 'share',
	        //'download',
	        //'zoom',
	        'close'
		    ],
			});
  };

  $('.closeBtn').click(function(e){
  	e.preventDefault();
  	var trLeng = $(this).parents('tbody').find('tr').length;
  	if (trLeng == 1) {
  		$('.cartTableTotals, .cartSubmit').hide();
  	}
  	$(this).parents('tr').fadeOut(200, function(){
  		$(this).remove();
  	});
  });

  $('.formControl .field input, .formControl .field textarea').focus(function(){
  	$(this).parents('.formControl').addClass('focused');
  });
  $('.formControl .field input, .formControl .field textarea').blur(function(){
  	$(this).parents('.formControl').removeClass('focused');
  });

  $(function(){
		$('.timer').each(function(){
			var _ = $(this);
			var min = Number(_.find('.minute').text());
			var sec = Number(_.find('.secound').text());
			var interval;
			interval = setInterval(function(){
				if (sec == 0) {
					sec = 60;
					if (min == 0) {
						clearInterval(interval);
						_.parents('.fieldText').hide();
						$('#codeField').hide();
						$('.btnBack').removeClass('hidden');
						$('.right').addClass('hidden');
						$('.alert-error-sms').append('*Время истекло. Пожалуйста, повторите запрос еще раз.');
						return false;
					}
					min--; 
				}
				sec--;
				secText = (sec<10)?"0"+sec:sec;
				minText = (min<10)?"0"+min:min;
				_.find('.minute').text(minText);
				_.find('.secound').text(secText);
			},1000);
		});
	});

	$('.scrollBar').mCustomScrollbar();
  
});