(function($) {
	$.fn.tabsScroll = function(){
		var _ = $(this);
		function scrollItem(){
			if(_.length > 0){
				var active = _.find('li.activeItem');
				var left = active.position().left - 50;
				_.animate({scrollLeft: left},200);		
			}
		}
		$(function(){
			scrollItem();
		});
		_.find('a').click(function(){
			scrollItem();
		});
	}
})(jQuery);
$(document).ready(function(){
	// MEDIA
	function media(){
		if($(window).width() <= 900){
			// $('.productTabNav').tabsScroll();
		}else{

		}
	}
	media();
	$(window).on('resize',function(){
		media();
	});

});