// JS for Product - AddFilter - Colors
$(document).on("click", ".addfilter-a", function (e) {
    e.preventDefault();
    var element = $(this);
    if (element.hasClass('addfilter-color')) {
        element.removeClass('addfilter-color');
    }
    else {
        $('.addfilter-a').removeClass('addfilter-color');
        element.addClass('addfilter-color');
    }

});
// END JS for Product - AddFilter - Colors

// JS for CART
$(document).on("click", ".add-to-cart", function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    qty = $('#qty').val();
    $.ajax({
        url: '/product/cartadd',
        data: {id: id, qty: qty},
        type: 'GET',
        success: function (res) {
            location.reload();
            alert('Спасибо! Ваш продукт добавлен в корзину');
        },
        error: function () {
            alert('Ошибка при добавлении в корзину');
        }
    })


});
$(document).on("click", ".add-to-cart-prod", function (e) {
    e.preventDefault();
    var id = $(this).data('id');

    $.ajax({
        url: '/product/cartadd',
        data: {id: id},
        type: 'GET',
        success: function (res) {
             location.reload();
            // alert('Спасибо! Ваш продукт добавлен в корзину');
        },
        error: function () {
            alert('Ошибка при добавлении в корзину');
        }
    })


});
$(document).on("click", ".minus-to-cart-prod", function (e) {
    e.preventDefault();
    var id = $(this).data('id');

    $.ajax({
        url: '/product/cart-minus',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            location.reload();
            // alert('Спасибо! Ваш продукт добавлен в корзину');
        },
        error: function () {
            alert('Ошибка при добавлении в корзину');
        }
    })


});

$(document).on('click', '.del-item', function () {

    var id = $(this).data('id');

    $.ajax({
        url: '/product/del-item',
        data: {id: id},
        type: 'GET',
        success: function (res) {
             location.reload();
            //alert('Ваш продукт удалено из корзины');
        },
        error: function () {
            alert('Ошибка при удалении');
        }
    });

});
// END CART

// FILTER
$(document).on('click', '.filter-items a', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var element = $(this);
    runLoader();
    $.get('/site/get-prods', {id: id}, function (res) {
        element.parents('li').siblings('li').removeClass('activeItem');
        element.parents('li').addClass('activeItem');
        element.parents('.sectionHead').siblings('.productsList').html(res);
        stopLoader();
    })
});
$(document).on('click', '.sort-category a', function (e) {
    e.preventDefault();

    var sort = $(this).data('sort');
    var input = '<input type="hidden" name="sort" value="' + sort + '">';
    var form = $('#filter-coco');
    var element = $(this);
    form.append(input);
    form.submit();
    element.parents('li').siblings('li').removeClass('activeItem');
    element.parents('li').addClass('activeItem');
});

$(document).on('click', '.filter_product', function (e) {
    e.preventDefault();
    var id = $(this).data('filter');
    var category = $(this).data('brand');
    var element = $(this);
    runLoader();
    $.get('/site/filter-prods', {id: id, category: category}, function (res) {
        element.parents('li').siblings('li').removeClass('activeItem');
        element.parents('li').addClass('activeItem');
        element.parents('.sectionHead').siblings('.productsList').html(res);
        stopLoader();
    })
});
// END FILTER

//LOADing
function runLoader() {
    $('.loader').removeClass('hidden');
}

function stopLoader() {
    $('.loader').addClass('hidden');
}

// Category Filter

$('.filterCheckList input').change(function (e) {
    e.preventDefault();
    var name = $(this).attr('name');
    var value = $(this).val();
    var text = $(this).parent().siblings('span').text();
    var tag = '<li data-name="' + name + '" data-value="' + value + '"><a href="#"><span>' + text + '</span></a></li>';
    $('#filter-coco').submit();
    if ($(this).prop('checked')) {
        $('.selectedList').append(tag);
    } else {
        $('.selectedList li[data-name="' + name + '"][data-value="' + value + '"]').remove();
    }
});


$(document).on('click', '.selectedList li', function (e) {

    e.preventDefault();
    var name = $(this).data('name');
    var id = $(this).data('value');
    var input = $('input:checkbox[name="' + name + '"][value="' + id + '"]');
    input.prop('checked', false);
    input.parent().removeClass('checked');
    $('.selectedList li[data-name="' + name + '"][data-value="' + id + '"]').remove();

    $('#filter-coco').submit();


});

$(document).on('click', '.removelallfilter', function (e) {

    e.preventDefault();


    var input = $('input');
    input.prop('checked', false);
    $('.jq-checkbox').removeClass('checked');
    $('.selectedList li').remove();

    $('#filter-coco').submit();


});

$('#filter-coco').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        type: 'GET',
        beforeSend: function () {
            runLoader();
        },
        url: '/product/cat-filter', //Change
        data: form.serialize(),
        success: function (res) {
            $('.productsList ul').html(res.content);
            window.history.pushState({}, "", res.url);
        },
        complete: function () {
            stopLoader();
        }
    });
});

// END Category Filter


// CHECK SMS
//
// $(document).on('click', '.ckecksms', function (e) {
//     e.preventDefault();
//
//     $.ajax({
//         url: '/product/checksms',
//         type: 'GET',
//         success: function (res) {
//         },
//         error: function () {
//             alert('Неправильный код');
//         }
//     })
//
//
// });


// END CHECK SMS
